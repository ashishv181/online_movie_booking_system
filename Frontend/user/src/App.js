import "./App.css";
import FooterPage from "./CommonPages/Footer";
import NavBar from "./Component/UserComponent/NavBar.js";
import { useState } from "react";
import AuthContext from "./Common/auth-context";
import SignIn from "./CommonPages/SignIn-Up-Edit/Signin";
import SignUp from "./CommonPages/SignIn-Up-Edit/Signup";
import AdminNavBar from "./Component/AdminComponent/Components/Header";
import EmployeeNavBar from "./Component/EmployeeComponents/EmployeeNavBar";
import ManagerNavBar from "./Component/ManagerComponent/Components/ManagerNavBar";

function App() {
  // let signedInUser = JSON.parse(localStorage.getItem("myUser"));

  const [user, setUser] = useState(JSON.parse(localStorage.getItem("myUser")));
  // const [role, setRole] = useState(JSON.parse(localStorage.getItem("role")));

  // const user = JSON.parse(localStorage.getItem("myUser"));
  const role = JSON.parse(localStorage.getItem("role"));

  // const logOutHandler = () => {
  //   localStorage.removeItem("myUser");
  //   setUser(null);
  // };

  function navBarToRender() {
    if (role === "admin") {
      console.log("admin");
      return <AdminNavBar />;
    } else if (role === "manager") {
      console.log("manager");
      return <ManagerNavBar />;
    } else if (role === "employee") {
      console.log("emp");
      return <EmployeeNavBar />;
    } else {
      return <NavBar />;
    }
  }

  return (
    <div>
      <div className="mainContainer">
        <AuthContext.Provider value={{ user, setUser }}>
          {user == null ? (
            <div className="loginDiv main-container">
              <SignIn />
              <SignUp />
            </div>
          ) : (
            navBarToRender()
          )}
        </AuthContext.Provider>
      </div>
      <FooterPage />
      {/* {user != null && <FooterPage />} */}
    </div>
  );
}
export default App;
