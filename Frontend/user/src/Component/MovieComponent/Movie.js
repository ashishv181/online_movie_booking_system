import "./css/Movie.css";
import { useState, useEffect } from "react";
import Carousel from "react-elastic-carousel";
import Item from "./Item";
import { url } from "../../Common/common";
import axios from "axios";

const Movie = () => {
  // const breakPoints = [
  //   { width: 1, itemsToShow: 1 },
  //   { width: 300, itemsToShow: 2 },
  //   { width: 300, itemsToShow: 3 },
  //   { width: 300, itemsToShow: 4 },
  //   { width: 300, itemsToShow: 5 },
  // ];
  const [movies, setMovies] = useState([]);

  useEffect(() => {
    getMovies();
  }, []);

  const getMovies = () => {
    axios.get(url + "/movie/getMovies").then((response) => {
      const result = response.data;
      if (result.status != null) {
        console.log("successfully retrieved movies");
        setMovies(result.status);
      } else {
        alert("error fetching movies");
      }
    });
  };

  return (
    <>
      <div className="">
        <div className="carousel-header">Now Playing</div>
        <Carousel breakPoints={breakPoints}>
          {movies.map((attr) => {
            return (
              <Item>
                <img
                  className="imageStyle"
                  src={url + "/image/" + attr.movieImage}
                  alt="avatar-img"
                />
              </Item>
            );
          })}
        </Carousel>
      </div>
      <div className="App">
        <div className="carousel-header">Now Playing</div>
        <Carousel breakPoints={breakPoints}>
          {movies.map((attr) => {
            return (
              <Item
                movie={attr.movieId}
                name={attr.movieName}
                description={attr.movieDescription}
                image={attr.movieImage}
                trailer="mytrailer"
              >
                <div></div>
              </Item>
            );
          })}
        </Carousel>
      </div>
    </>
  );
};

export default Movie;

// const DUMMY_MOVIES = [
//   {
//     key: "!",
//     movie_name: "Boss Baby",
//     movie_image: img3,
//     movie_description: "A baby whose the boss of all babies",
//     movie_runtime: "2:45",
//     movie_age_limit: "U",
//   },
//   {
//     key: "2",
//     movie_name: "Escape Room",
//     movie_image: img1,
//     movie_description: "Tournament of Champions",
//     movie_runtime: "2:54",
//     movie_age_limit: "A",
//   },
//   {
//     key: "3",
//     movie_name: "Life of Pi",
//     movie_image: img8,
//     movie_description: "Stranded alone",
//     movie_runtime: "2:54",
//     movie_age_limit: "A",
//   },
//   {
//     key: "4",
//     movie_name: "SlumDog Millionaire",
//     movie_image: img7,
//     movie_description: "Its all on fate",
//     movie_runtime: "2:54",
//     movie_age_limit: "A",
//   },
//   {
//     key: "5",
//     movie_name: "The Dark Knight",
//     movie_image: img6,
//     movie_description: "Saviour of Gotham",
//     movie_runtime: "2:54",
//     movie_age_limit: "A",
//   },
//   {
//     key: "6",
//     movie_name: "Fast and Furious 9",
//     movie_image: img2,
//     movie_description: "A never ending movie series",
//     movie_runtime: "2:35",
//     movie_age_limit: "A",
//   },
//   {
//     key: "7",
//     movie_name: "Hitman and BodyGuard 2",
//     movie_image: img4,
//     movie_description: "A baby whose the boss of all babies",
//     movie_runtime: "1:45",
//     movie_age_limit: "A",
//   },
//   {
//     key: "8",
//     movie_name: "Still Water",
//     movie_image: img5,
//     movie_description: "Water is not moving",
//     movie_runtime: "2:23",
//     movie_age_limit: "U",
//   },
// ];
