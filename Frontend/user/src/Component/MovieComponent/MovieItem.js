import { useState } from "react";
import { NavLink, Route } from "react-router-dom";
import { url } from "../../Common/common";
import axios from "axios";
import classes from "./css/Item.module.css";
import MovieDetail from "./MovieInfo";
import { useHistory } from "react-router";

const Item = ({ movie, name, description, image, trailer }) => {
  const history = useHistory();
  const [mouseEntered, setMouseEntered] = useState(false);
  const [clicked, setIsClicked] = useState(false);

  const mouseEnterkHandler = () => {
    setMouseEntered(true);
  };
  const mouseLeaveHandler = () => {
    setMouseEntered(false);
  };

  const clickHandler = () => {
    setIsClicked(true);
  };

  const getInfoOfSelectedMovie = () => {
    axios.get(url + "/movie/info/" + movie).then((response) => {
      const result = response.data;
      if (result.status != null) {
        history.push("/movie-info", { moviexxx: result.status });
      } else {
        alert("error occured while getting movie detail");
      }
    });
  };

  return (
    <div>
      <div
        onMouseEnter={mouseEnterkHandler}
        onMouseLeave={mouseLeaveHandler}
        className={classes.styledDiv}
        onClick={getInfoOfSelectedMovie}
      >
        <img className={classes.image} src={url + "/image/" + image} />
        {mouseEntered && <div className={classes.movieName}>{name}</div>}
        {/* {mouseEntered && <MovieDetail />} */}
      </div>
    </div>
  );
};

export default Item;
