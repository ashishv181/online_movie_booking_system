import { useLocation, useHistory } from "react-router";
import { useState } from "react";
import { useEffect } from "react";
import axios from "axios";
import { url } from "../../Common/common";
import "./css/MovieInfo.css";
// import { YoutubePlayer } from "reactjs-media";
import YouTube from "react-youtube";
import ReactPlayer from "react-player";

const MovieInfo = () => {
  const location = useLocation();
  const movie = location.state.moviexxx;

  console.log(movie);

  const [shows, setShows] = useState([]);
  useEffect(() => {
    // showDetailsforMovie();
  }, [shows]);

  const history = useHistory();

  const showDetailsforMovie = () => {
    axios.get(url + "/show/getShow/" + movie.movieId).then((response) => {
      const result = response.data;
      if (result != null) {
        setShows([...result]);
      } else {
        alert("error occured while getting shows");
      }
    });
  };

  const theatreRedirect = (shw) => {
    // console.log("showId: " + shw.showId);
    // console.log("the id: " + shw.theatre.theatreId);
    history.push("/theatre-info", {
      showId: shw.showId,
      movieId: shw.movie.movieId,
      theatreId: shw.theatre.theatreId,
    });
  };

  return (
    <div className="MainVideo">
      {movie.movieTrailer != null && (
        <div className="trailer">
          <ReactPlayer
            url={movie.movieTrailer} // Reqiured
            controls="true"
            width={900}
            height={400}
          />
        </div>
      )}
      {/* <div className="info"><b>{movie.movieTrailer}</b></div> */}
      <div className="info">
        <b>{movie.movieDescription}</b>
      </div>
      <div className="ageLimit">
        <h2>
          <b>{movie.ageLimit}</b>
        </h2>
      </div>

      <div>
        <button onClick={showDetailsforMovie}>Get Shows</button>
      </div>
      <div className="showListContained container">
        <table className="table table-borderless">
          <thead>
            <th className="tableColumn">Movie Name</th>
            <th className="tableColumn">Theatre Name</th>
            <th className="tableColumn">Show Start Time</th>
            <th className="tableColumn">Show End Time</th>
            <th className="tableColumn"></th>
          </thead>
          <tbody>
            {shows.map((shw) => {
              return (
                <tr>
                  <td>{shw.movie.movieName}</td>
                  <td>{shw.theatre.theatreName}</td>
                  <td>{shw.showStartTime}</td>
                  <td>{shw.showEndTime}</td>

                  <td>
                    <button
                      onClick={() => {
                        theatreRedirect(shw);
                      }}
                      id={shw.theatre.theatreId}
                    >
                      Book
                    </button>{" "}
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default MovieInfo;
