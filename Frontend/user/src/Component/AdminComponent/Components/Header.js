import { BrowserRouter, Link, Route, Switch, NavLink } from "react-router-dom";
import img2 from "../../../Images/userBar.png";
import "../../UserComponent/css/NavBar.css";

import Manager from "./Manager";
// import Signin from "./Signin";
import Theatre from "./Theatre";
import AuthContext from "../../../Common/auth-context";
import { useState, useContext } from "react";
import Updatemanager from "./Updatemanager";
import Updatetheatre from "./Updatetheatre";
// import "../CSS/Home.css";
import Editprofile from "../../../CommonPages/SignIn-Up-Edit/EditProfile";
import Offer from "./Offer";

const HeaderPage = () => {
  const { user, setUser } = useContext(AuthContext);
  const [firstName, setName] = useState(user.adminName.split(" ")[0]);

  const logOutHandler = () => {
    localStorage.removeItem("myUser");
    localStorage.removeItem("role");
    localStorage.removeItem("theatre");
    localStorage.removeItem("manager");
    setUser(null);
  };

  return (
    <div>
      <BrowserRouter>
        <nav className="navbar navbar-expand-lg navbar-dark navContainer">
          <div className="container-fluid">
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarNav"
              aria-controls="navbarNav"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
              <ul className="navbar-nav">
                <li className="nav-item">
                  <Link className="nav-link" to="/manager">
                    <button className="btn btn-dark">Manager</button>
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/theatre">
                    <button className="btn btn-dark">Theatre</button>
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/feedback">
                    <button className="btn btn-dark">Feedback</button>
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/offer">
                    <h4 className="btn btn-dark"> Offer</h4>
                  </Link>
                </li>
                <div className="flex">
                  <li className="nav-item dropDownContainer dropdown">
                    <a
                      className="nav-link  {/*dropdown-toggle*/} "
                      href="#"
                      id="navbarDropdownMenuLink"
                      role="button"
                      data-bs-toggle="dropdown"
                      aria-expanded="false"
                    >
                      {user != null && (
                        <div className="userName">Hi {firstName}!</div>
                      )}
                    </a>
                    <ul
                      className="dropdown-menu dropDownContent dropdown-menu-end "
                      aria-labelledby="navbarDropdownMenuLink"
                    >
                      <li>
                        <NavLink
                          className="nav-link dropdown-item myDropdown"
                          to="/editprofile"
                        >
                          <b>Edit Profile</b>
                        </NavLink>
                      </li>
                      <li>
                        <div
                          onClick={logOutHandler}
                          className="nav-link dropdown-item myDropdown"
                          to="/signout"
                        >
                          <b>Sign Out</b>
                        </div>
                      </li>
                    </ul>
                  </li>
                </div>
              </ul>
            </div>
          </div>
        </nav>

        <Switch>
          <Route path="/manager" component={Manager} />
          <Route path="/theatre" component={Theatre} />
          <Route path="/offer" component={Offer} />
          <Route path="/editprofile" component={Editprofile} />
          <Route path="/update" component={Updatemanager} />
          <Route path="/updatetheatre" component={Updatetheatre} />
        </Switch>
      </BrowserRouter>
    </div>
  );
};

export default HeaderPage;

// const HeaderPage = () =>{
//   return(
//       <BrowserRouter>
//       <div>

//   <div className="header">
//       <div className="profile">
//           <a href="editprofile"><img className="image" src={img1} alt="Profile"></img></a>

//       </div>

//   </div>
//   <nav className="navbar navbar-expand-lg navbar-light bg-warning" ></nav>

//    <div className="container-fluid">
//      <button><b>Book Show</b></button>
//    </div>

//    <div className="container-fluid">
//      <form className="d-flex">
//        <b><input className="form-control me-2" type="search" placeholder="Search" aria-label="Search"/></b>
//        <b><button className="btn btn-outline-success" type="submit"><b>Search</b></button></b>
//      </form>

//      <div className="centrealign">

//           <table className="table-responsive">

//               <tr>
//                   <td className="columndimen">

//                   <Link className="btn btn-primary btn-lg" to="/manager">manager</Link>

//                   </td>
//                   <td className="columndimen">

//                   <a class="btn btn-primary btn-lg" href="theatre">Theatre</a>
//                   </td>
//                   <td className="columndimen">

//                   <a class="btn btn-primary btn-lg" href="offer">App Offer</a>
//                   </td>
//                   <td className="columndimen">

//                   <a class="btn btn-primary btn-lg" href="revenue">Revenue</a>
//                   </td>
//                   <td className="columndimen">

//                   <a class="btn btn-primary btn-lg" href="feedback">Feedback</a>
//                   </td>
//               </tr>
//                </table>
//           <Switch>
//         <Route path="/manager" component={Manager} />
//         <Route path="/theatre" component={Trial} />
//         <Route path="/offer" component={Trial} />
//         <Route path="/revenue" component={Trial} />
//         <Route path="/feedback" component={Trial} />
//         <Route path="/editprofile" component={Trial} />
//       </Switch>

//      </div>

//  </div>

//   </div>
//   </BrowserRouter>
//   )

// }

//****************************************************************************************** */
{
  /* <div>
<BrowserRouter>
  <nav className="navbar navbar-expand-lg navbar-light bg-light">
    <div className="head">
      <a className="navbar-brand" href="#">
       <h2 className="font"> WELCOME TO BOOK MY SHOW !!!!</h2>
      </a>
      <div className="profile">
  <a href="editprofile"><img className="image" src={img1} alt="Profile"></img></a>

</div>
      <button
        className="navbar-toggler"
        type="button"
        data-bs-toggle="collapse"
        data-bs-target="#navbarNav"
        aria-controls="navbarNav"
        aria-expanded="false"
        aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav">
          <li className="nav-item">
            <Link className="nav-link" to="/manager">
             <h4 className="btn btn-primary btn-lg"> Manager</h4>
            </Link>
          </li>
          <li>
            <Link className="nav-link" to="/theatre">
            <h4 className="btn btn-primary btn-lg"> Theatre</h4>
            </Link>
          </li>
          <li>
            <Link className="nav-link" to="/offer">
            <h4 className="btn btn-primary btn-lg"> Offer</h4>
            </Link>
          </li>
          <li>
            <Link className="nav-link" to="/feedback">
            <h4 className="btn btn-primary btn-lg"> Feedback</h4>
            </Link>
          </li>
          <li>
            <Link className="nav-link" to="/revenue">
            <h4 className="btn btn-primary btn-lg"> Revenue</h4>
            </Link>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <div className="container">
  <Switch>
<Route path="/manager" component={Manager} />
  <Route path="/theatre" component={Trial} />
  <Route path="/offer" component={Trial} />
  <Route path="/revenue" component={Trial} />
  <Route path="/feedback" component={Trial} />
  <Route path="/editprofile" component={Trial} />
</Switch>
  </div>
</BrowserRouter>
</div> */
}
