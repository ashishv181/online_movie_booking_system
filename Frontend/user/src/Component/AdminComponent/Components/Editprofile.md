import { BrowserRouter, Link, Route, Switch } from 'react-router-dom'

import { useEffect, useState } from 'react';
import axios from 'axios';
import { url } from './Constant';
import '../CSS/Update.css'

import { useHistory } from 'react-router-dom'

const Editprofile = () => {
    const Admin = JSON.parse(localStorage.getItem("myAdmin"))
    console.log(Admin)
const [name, setName] = useState(Admin.name)
const [mobileNo, setMobileNo] = useState(Admin.mobileNo)
const [email, setEmail] = useState(Admin.email)
const [password, setPassword] = useState(Admin.password)
const history = useHistory();
const id = Admin.adminId;
console.log(id)
 

   const updateProfileDB = () => {
    if (email.length === 0) {
        alert('please enter email')
      } else if (password.length === 0) {
        alert('please enter password')
      } else if (mobileNo.length === 0) {
        alert('please enter mobile No')
      }else if (name.length === 0) {
        alert('please enter Name')
      }else{
       const data = new FormData();
       data.append("email",email)
       data.append("password",password)
       data.append("mobileNo",mobileNo)
       data.append("name",name)
       data.append("adminId",id)
  
       axios.put(url + '/admin/'+ id, data ).then((response) => {
        console.log("put method call")
        const result = response.data
        if (result.status === 'success') {
            alert('successfully Updated')
            history.push('/')
        } else {
            alert('error while updating')
        }
    })
}
    }


    return (
        
    
      <div className="centrealign container">
       
       <table className="tablepadding " >
                                <tr>
                                    <td>
                                        <input type="text" value={name} onChange={(e) => {
                                            setName(e.target.value)  
                                        }} class="form-control" placeholder="Name" /><br />
                                        <input type="text" value={mobileNo} onChange={(e) => {
                                            setMobileNo(e.target.value)
                                        }} class="form-control"   placeholder="Mobile No" /><br />
                                        <input type="Email" value={email} onChange={(e) => {
                                            setEmail(e.target.value)
                                        }} class="form-control"   placeholder="Email" /><br />
                                        <input type="Password" value={password} onChange={(e) => {
                                            setPassword(e.target.value)
                                        }} class="form-control"   placeholder="Password" /><br />
                                     
                                        <button className="btn btn-secondary btn-lg addbutton" onClick={updateProfileDB}>Update</button>
                                       

                                    </td>
                                </tr>
                            </table>
    
    

        
      </div>
    )
  }
  
  export default Editprofile