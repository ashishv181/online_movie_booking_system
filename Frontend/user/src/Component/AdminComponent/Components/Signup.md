
import { useState } from 'react'
import { useHistory } from 'react-router'
import axios from 'axios'
import { url } from './Constant'
import '../CSS/Cred.css'

const Signup = () => {
  // define the state
 
  const [name, setName] = useState('')
  const [mobileNo, setMobileNo] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const history = useHistory();

  const signupAdmin = () => {

    
    if (email.length === 0) {
      alert('please enter email')
    } else if (password.length === 0) {
      alert('please enter password')
    } else if (mobileNo.length === 0) {
      alert('please enter mobile No')
    }else if (name.length === 0) {
      alert('please enter Name')
    }else{
     const data = new FormData();
     data.append("email",email)
     data.append("password",password)
     data.append("mobileNo",mobileNo)
     data.append("name",name)

     axios.post(url + "/admin/signupadmin", data).then((response)=>{
       const result=response.data;
       if(result.status === "error"){
        alert("error")
       }else{
        
         alert("Successfuly Signup")
       // localStorage.setItem("myAdmin", JSON.stringify(result.status));
        history.push("/signin");
       }
     })
    }
  }
  return (
    <div className="container background_colour ccentered signinup col-sm-8">
   
    
      <div >
       Name
        <input
          onChange={(event) => {
            // updating the state with user entered valuex
            setName(event.target.value)
          }}
          className="form-control"
          type="text"
        />
      </div>
  
      <div >
        <label>Email</label>
        <input
          onChange={(event) => {
            // updating the state with user entered value
            setEmail(event.target.value)
          }}
          className="form-control"
          type="email"
        />
      </div>
      <div >
        <label>Password</label>
        <input
          onChange={(event) => {
            // updating the state with user entered value
            setPassword(event.target.value)
          }}
          className="form-control"
          type="password"
        />
      </div>
      <div className="mb-3">
        <label>Mobile No</label>
        <input
          onChange={(event) => {
            // updating the state with user entered value
            setMobileNo(event.target.value)
          }}
          className="form-control"
          type="number"
        />
      </div>
      <div className="mb-3 d-grid gap-2 col-2 mx-auto">
        <button onClick={signupAdmin} className="btn btn-primary btn-lg">
          Signup
        </button>
      </div>
    </div>
  )
}

export default Signup