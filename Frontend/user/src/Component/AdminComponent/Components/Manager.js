import { BrowserRouter, Link, Route, Switch } from "react-router-dom";
// import "../CSS/Manager.css";
import { useEffect, useState } from "react";
import axios from "axios";
import { url } from "../../../Common/common";
import "../../ManagerComponent/css/List.css";
import trashImg from "../../../Images/trash.png";
import updateImg from "../../../Images/updated.png";

import { useHistory } from "react-router-dom";

const Manager = () => {
  //   const Admin = JSON.parse(localStorage.getItem("myAdmin"));

  const history = useHistory();

  const [managerName, setManagerName] = useState("");
  const [managerMobile, setManagerMobile] = useState("");
  const [managerEmail, setManagerEmail] = useState("");
  const [managerPassword, setManagerPassword] = useState("");
  const [managerDob, setManagerDob] = useState("");
  const [theatre, setTheatre] = useState(0);
  const [changesMade, setChangesMade] = useState(false);

  const [manager, setManager] = useState([]);
  //   const [managerisdeleted, setManagerisdeleted] = useState(false);

  useEffect(() => {
    getAllManager();
  }, [changesMade]);

  const accessmanager = (e) => {
    const id = e.target.id;
    axios.get(url + "/manager/" + id).then((response) => {
      const result = response.data;

      if (result.status != null) {
        alert("successfully access manager");
        localStorage.setItem("manager", JSON.stringify(result.status));
        console.log(result.status);
        history.push("/update");
      } else {
        alert("error while accessing  manager");
      }
    });
  };

  const deletemanager = (e) => {
    const id = e.target.id;
    axios.delete(url + "/manager/" + id).then((response) => {
      const result = response.data;
      if (result.status === "success") {
        {
          changesMade ? setChangesMade(false) : setChangesMade(true);
        }
        alert("successfully deleted an manager");
      } else {
        alert("error while deleting  manager");
      }
    });
  };

  const getAllManager = () => {
    console.log("called");
    axios.get(url + "/manager/getManagers").then((response) => {
      console.log("res: " + response.data);
      const res = response.data;
      setManager(res);
    });
  };

  const addManagerToDB = () => {
    if (managerName.length === 0) {
      alert("Enter Name");
    } else if (managerMobile.length === 0) {
      alert("Enter MobileNo");
    } else if (managerEmail.length === 0) {
      alert("Enter Email");
    } else if (managerPassword.length === 0) {
      alert("Enter Password");
    } else if (managerDob.length === 0) {
      alert("Enter DOB");
    } else if (!theatre) {
      alert("Enter TheatreId");
    } else {
      const data = new FormData();
      data.append("managerName", managerName);
      data.append("managerMobile", managerMobile);
      data.append("managerEmail", managerEmail);
      data.append("managerPassword", managerPassword);
      data.append("managerDob", managerDob);
      data.append("theatre", theatre);

      axios.post(url + "/manager/add", data).then((response) => {
        const result = response.data;
        if (result.status == "success") {
          {
            changesMade ? setChangesMade(false) : setChangesMade(true);
          }
          alert("successfully added manager");
        } else {
          alert("error while adding manager");
        }
      });
    }
  };
  return (
    <div className="mainContainerDiv">
      <div className="formDiv">
        <div className="inputDiv">
          <input
            type="text"
            onChange={(e) => {
              setManagerName(e.target.value);
            }}
            class="form-control"
            placeholder="Name"
          />
        </div>
        <div className="inputDiv">
          <input
            type="text"
            onChange={(e) => {
              setManagerMobile(e.target.value);
            }}
            class="form-control"
            placeholder="Mobile No"
          />
        </div>
        <div className="inputDiv">
          <input
            type="Email"
            onChange={(e) => {
              setManagerEmail(e.target.value);
            }}
            class="form-control"
            placeholder="Email"
          />
        </div>
        <div className="inputDiv">
          <input
            type="Password"
            onChange={(e) => {
              setManagerPassword(e.target.value);
            }}
            class="form-control"
            placeholder="Password"
          />
        </div>
        <div className="inputDiv">
          <input
            type="date"
            onChange={(e) => {
              setManagerDob(e.target.value);
            }}
            class="form-control"
            placeholder="Date of Birth"
          />
        </div>
        <div className="inputDiv">
          <input
            type="number"
            onChange={(e) => {
              setTheatre(e.target.value);
            }}
            class="form-control"
            placeholder="Theatre Id"
          />
        </div>
        <div onClick={addManagerToDB} className="submitBtn">
          Submit
        </div>
      </div>
      <div className="fetchedDataDiv">
        <div className="titleDiv">
          <div className="movieIddiv">Manager Id</div>
          <div className="movieNamediv">Manager Name</div>
          <div className="deleteBtndiv">Delete</div>
          <div className="updateBtndiv">Update</div>
        </div>
        <div className="itemList">
          {manager.map((man) => {
            return (
              <div className="itemRow">
                <div className="movieIddiv">{man.managerId}</div>
                <div className="movieNamediv">{man.managerName}</div>
                <div className="deleteBtndiv">
                  <img
                    className="deleteImg"
                    id={man.managerId}
                    onClick={deletemanager}
                    src={trashImg}
                    alt="delete"
                  />
                </div>
                <div className="updateBtndiv">
                  <img
                    className="updateImg"
                    id={man.managerId}
                    onClick={accessmanager}
                    src={updateImg}
                    alt="update"
                  />
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};
export default Manager;

/* 
return (
    <div className="container table1">
      <table>
        <tr>
          <td>
            <div class="form-floating mb-6">
              <table className="tablepadding">
                <tr>
                  <td>
                    <input
                      type="text"
                      onChange={(e) => {
                        setManagerName(e.target.value);
                      }}
                      class="form-control"
                      placeholder="Name"
                    />
                    <br />
                    <input
                      type="text"
                      onChange={(e) => {
                        setManagerMobile(e.target.value);
                      }}
                      class="form-control"
                      placeholder="Mobile No"
                    />
                    <br />
                    <input
                      type="Email"
                      onChange={(e) => {
                        setManagerEmail(e.target.value);
                      }}
                      class="form-control"
                      placeholder="Email"
                    />
                    <br />
                    <input
                      type="Password"
                      onChange={(e) => {
                        setManagerPassword(e.target.value);
                      }}
                      class="form-control"
                      placeholder="Password"
                    />
                    <br />
                    <input
                      type="date"
                      onChange={(e) => {
                        setManagerDob(e.target.value);
                      }}
                      class="form-control"
                      placeholder="Date of Birth"
                    />
                    <br />
                    <input
                      type="number"
                      onChange={(e) => {
                        setTheatre(e.target.value);
                      }}
                      class="form-control"
                      placeholder="Theatre Id"
                    />
                    <br />
                    <button
                      className="btn btn-secondary btn-lg addbutton"
                      onClick={addManagerToDB}
                    >
                      Add
                    </button>
                  </td>
                </tr>
              </table>
            </div>
          </td>
          <td>
            <div className="table2 ">
              <table>
                <thead>
                  <tr>
                    <th className="tablecolumn">Manager Id</th>
                    <th className="tablecolumn">Name</th>
                    <th className="tablecolumn">Delete</th>
                    <th className="tablecolumn">Update</th>
                  </tr>
                </thead>
                <tbody>
                  {manager.map((man) => {
                    return (
                      <tr>
                        <td> {man.managerId} </td>
                        <td>{man.managerName} </td>
                        <td>
                          {" "}
                          <button
                            className="btn btn-secondary"
                            id={man.managerId}
                            onClick={deletemanager}
                          >
                            delete
                          </button>
                        </td>
                        <td>
                          {" "}
                          <button
                            className="btn btn-secondary"
                            id={man.managerId}
                            onClick={accessmanager}
                          >
                            Update
                          </button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </td>
        </tr>
      </table>
    </div>
  );
}; */

/* <div className="verticle">
    
    
    <BrowserRouter>
    
    <div className="div">
     
    <Link className="btn btn-primary btn-sm" to="/addmanager">Add Manager</Link>
    </div>
    <br></br>
    <div className="div">
    <Link className="btn btn-primary btn-sm" to="/deletemanager">Delete Manager</Link>
    </div>
    <br></br>
    <div className="div">
    <Link className="btn btn-primary btn-sm" to="/updateemanager">Update Manager</Link>
    </div>
  
   
    <Switch>
    <Route path="/addmanager" component={Trial} />
    <Route path="/deletemanager" component={Trial} />
    <Route path="/updateemanager" component={Trial} />
        
    </Switch>
    </BrowserRouter>

  </div> */
