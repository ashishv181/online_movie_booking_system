
import { useEffect } from "react";
import { useHistory } from "react-router";

const Signout = () => {

  useEffect(() => {
    window.location.reload();
  }, [])

  const history = useHistory();
  localStorage.clear();
  history.push("/home");
  return (
    <div className="container ccentered ">

    </div>
  )
}

export default Signout



