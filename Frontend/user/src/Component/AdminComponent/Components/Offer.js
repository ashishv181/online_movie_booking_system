import { BrowserRouter, Link, Route, Switch } from "react-router-dom";
// import "../CSS/Manager.css";
import { useEffect, useState } from "react";
import axios from "axios";
import { url } from "../../../Common/common";
import "../../ManagerComponent/css/List.css";
import trashImg from "../../../Images/trash.png";
import updateImg from "../../../Images/updated.png";
import { useHistory } from "react-router-dom";

const Offer = () => {
  const history = useHistory();

  const [offerName, setOfferName] = useState("");
  const [offerDetails, setOfferDetails] = useState("");
  const [offer, setOffer] = useState([]);
  const [changesMade, setChangesMade] = useState(false);

  useEffect(() => {
    getaAllOffer();
  }, [changesMade]);

  const deleteoffer = (e) => {
    const id = e.target.id;
    axios.delete(url + "/offer/" + id).then((response) => {
      const result = response.data;
      if (result.status == "success") {
        {
          changesMade ? setChangesMade(false) : setChangesMade(true);
        }
        alert("successfully deleted an offer");
      } else {
        alert("error while deleting  offer");
      }
    });
  };
  
  const getaAllOffer = () => {
    axios.get(url + "/offer/getoffer").then((response) => {
      const result = response.data;
      setOffer(result);
    });
  };

  const addOfferToDB = () => {
    if (offerName.length === 0) {
      alert("Enter OfferName");
    } else if (offerDetails.length === 0) {
      alert("Enter OfferDetails");
    } else {
      const data = new FormData();

      data.append("offerName", offerName);
      data.append("offerDetails", offerDetails);

      axios.post(url + "/offer", data).then((response) => {
        const result = response.data;
        if (result.status === "success") {
          {
            changesMade ? setChangesMade(false) : setChangesMade(true);
          }
          alert("successfully added an offer");
        } else {
          alert("error while adding offer");
        }
      });
    }
  };

  return (
    <div className="mainContainerDiv">
      <div className="formDiv">
        <div className="inputDiv">
          <input
            type="text"
            onChange={(e) => {
              setOfferName(e.target.value);
            }}
            class="form-control"
            placeholder="Offer Name"
          />
        </div>
        <div className="inputDiv">
          <input
            type="text"
            onChange={(e) => {
              setOfferDetails(e.target.value);
            }}
            class="form-control"
            placeholder="Offer Details"
          />
        </div>
        <div onClick={addOfferToDB} className="submitBtn">
          Submit
        </div>
      </div>
      <div className="fetchedDataDiv">
        <div className="titleDiv">
          <div className="movieIddiv">Offer</div>
          <div className="movieNamediv">Offer Details</div>
          {/* <div className="movieNamediv">Theatre Name</div> */}
          <div className="deleteBtndiv">Delete</div>
        </div>
        <div className="itemList">
          {offer.map((off) => {
            return (
              <div className="itemRow">
                <div className="movieIddiv">{off.offerName}</div>
                <div className="movieNamediv">{off.offerDetails}</div>
                {/* <div className="movieNamediv">{thtr.theatreName}</div> */}
                <div className="deleteBtndiv">
                  <img
                    className="deleteImg"
                    id={off.offerId}
                    onClick={deleteoffer}
                    src={trashImg}
                    alt="delete"
                  />
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );

  return (
    <div className="container table1">
      <table>
        <tr>
          <td>
            <div class="form-floating mb-6">
              <table className="tablepadding">
                <tr>
                  <td>
                    <input
                      type="text"
                      onChange={(e) => {
                        setOfferName(e.target.value);
                      }}
                      class="form-control"
                      placeholder="Offer Name"
                    />
                    <br />
                    <input
                      type="text"
                      onChange={(e) => {
                        setOfferDetails(e.target.value);
                      }}
                      class="form-control"
                      placeholder="Offer Details"
                    />
                    <br />

                    <button
                      className="btn btn-secondary btn-lg addbutton"
                      onClick={addOfferToDB}
                    >
                      Add
                    </button>
                  </td>
                </tr>
              </table>
            </div>
          </td>
          <td>
            <div className="table2 ">
              <table>
                <thead>
                  <tr>
                    <th className="tablecolumn">Offer </th>
                    <th className="tablecolumn">Offer Details</th>
                    <th className="tablecolumn">Delete</th>
                  </tr>
                </thead>
                <tbody>
                  {offer.map((off) => {
                    return (
                      <tr>
                        <td> </td>
                        <td> </td>
                        <td>
                          {" "}
                          <button
                            className="btn btn-secondary"
                            id={off.offerId}
                            onClick={deleteoffer}
                          >
                            delete
                          </button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </td>
        </tr>
      </table>
    </div>
  );
};

export default Offer;

{
  /* <div className="verticle">
    
    
    <BrowserRouter>
    
    <div className="div">
     
    <Link className="btn btn-primary btn-sm" to="/addmanager">Add Manager</Link>
    </div>
    <br></br>
    <div className="div">
    <Link className="btn btn-primary btn-sm" to="/deletemanager">Delete Manager</Link>
    </div>
    <br></br>
    <div className="div">
    <Link className="btn btn-primary btn-sm" to="/updateemanager">Update Manager</Link>
    </div>
  
   
    <Switch>
    <Route path="/addmanager" component={Trial} />
    <Route path="/deletemanager" component={Trial} />
    <Route path="/updateemanager" component={Trial} />
        
    </Switch>
    </BrowserRouter>

  </div> */
}
