import { useState } from 'react'
import axios from "axios";
import { useHistory } from "react-router-dom";
import { url } from './Constant';
import '../CSS/Cred.css'


const Signin = () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const[isLoggedin,setIsLoggedin]=useState(false)
  const history = useHistory();


  const signinUser = () => {

    
    if (email.length === 0) {
      alert('please enter email')
    } else if (password.length === 0) {
      alert('please enter password')
    } else {
     const data = new FormData();
     data.append("email",email)
     data.append("password",password)

     axios.post(url + "/admin", data).then((response)=>{
       const result=response.data;
    
      
       if(result.status === null){
        alert("error")
       }else{
        
         alert("Successfuly Signin")
        
     localStorage.setItem("myAdmin", JSON.stringify(result.status));
      const setIsLoggedin = true;
      localStorage.setItem("isLoggedin", JSON.stringify(isLoggedin));
      console.log(isLoggedin)
      if(setIsLoggedin===true)
        history.push("/header");
       }
     })
    }
  }

  return (
    <div className="container background_ccolour ccentered signinup col-sm-8">
 <div className="mb-4">
        <label><h3>Email</h3></label>
        <input
          onChange={(event) => {
            setEmail(event.target.value)
          }}
          placeholder="Enter your email"
          className="form-control"
          type="email"
        />
      </div>
      <div className="mb-3">
        <label><h3>Password</h3></label>
        <input
          onChange={(event) => {
            setPassword(event.target.value)
          }}
          className="form-control"
          type="password"
        />
      </div>
      <div className="mb-3 d-grid gap-2 col-2 mx-auto">
        <button onClick={signinUser} className="btn btn-primary btn-lg">
          Signin
        </button>

         
        


      </div>
    </div>
  )
}

export default Signin


{/* <div className="container">
<br></br>
<h1>Enter Details</h1>


  <table className="table table-striped h2">
 <tr>
<td ><h4 >Email:</h4></td>     

<td><input 
  onChange={(event) => {
    setEmail(event.target.value)
  }}
  placeholder="Enter your email"
  className="form-control form-control-lg"
  type="email"
/></td>   
</tr>
<tr>
<td>
<h4>Password :</h4>
</td>
<td>
<input
  onChange={(event) => {
    setPassword(event.target.value)
  }}
  className="form-control form-control-lg"
  type="password"
/>
</td>
</tr>
<tr><td colspan="2"> <div className="mb-3 ">
<button onClick={signinUser} className="btn btn-primary">
  Signin
</button>
</div></td>
</tr>
  </table>
  </div>
 

) */}