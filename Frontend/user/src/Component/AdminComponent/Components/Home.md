import { BrowserRouter, Link, Route, Switch } from "react-router-dom";
import img2 from "../Images/profile.png";
import Trial from "./Trial";
import Manager from "./Manager";
import Signin from "./Signin";
import Signup from "./Signup";
import Theatre from "./Theatre";

import Updatetheatre from "./Updatetheatre";
import HeaderPage from "./Header";
import "../CSS/Home.css";

const Home = () => {
  
  return (
    <div className="container-fluid ">
      <BrowserRouter>
        <div className="img3">
          <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="headerfix">
              <button
                className="navbar-toggler"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#navbarNav"
                aria-controls="navbarNav"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="navbar-toggler-icon"></span>
              </button>
              <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="nav nav-tabs">
                  <div className="homeheader">
                    {" "}
                    <h1>WELCOME</h1>
                  </div>

                  <div className="flex">
                    <li>
                      <a href="editprofile">
                        <img className="image" src={img2} alt="Profile"></img>
                      </a>
                    </li>
                    <li>
                      <Link className="nav-link" to="/signin">
                        <h5>Signin</h5>
                      </Link>
                    </li>
                    <li className="nav-item dropdown">
                      <Link
                        className="nav-link dropdown-toggle "
                        data-bs-toggle="dropdown"
                        to="/signup"
                      >
                        <h5>Signup</h5>
                      </Link>
                      <ul className="dropdown-menu">
                        <li>
                          <Link class="dropdown-item" to="register">
                            Register
                          </Link>
                        </li>
                      </ul>
                    </li>
                  </div>
                </ul>
              </div>
            </div>
          </nav>

          <Switch>
            <Route path="/manager" component={Manager} />
            <Route path="/theatre" component={Theatre} />
            <Route path="/offer" component={Trial} />
            <Route path="/revenue" component={Trial} />
            <Route path="/feedback" component={Trial} />

            <Route path="/signin" component={Signin} />
            {/* <Route path="/signup" component={Signup} />  */}

            <Route path="/updatetheatre" component={Updatetheatre} />
            <Route path="/register" component={Signup} />
            <Route path="/header" component={HeaderPage} />
          </Switch>
        </div>
      </BrowserRouter>
    </div>
  );
};

export default Home;
