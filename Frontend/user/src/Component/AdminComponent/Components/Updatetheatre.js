import { BrowserRouter, Link, Route, Switch } from "react-router-dom";

import { useEffect, useState } from "react";
import axios from "axios";
import { url } from "../../../Common/common";
import "../../ManagerComponent/css/List.css";

import { useHistory } from "react-router-dom";

const Updatetheatre = () => {
  const Theatre = JSON.parse(localStorage.getItem("theatre"));
  //console.log(JSON.parse(Manager))

  const [theatreName, setTheatreName] = useState(Theatre.theatreName);
  const [theatreAddress, setTheatreAddress] = useState(Theatre.theatreAddress);
  const [theatreCity, setTheatreCity] = useState(Theatre.theatreCity);

  const id = Theatre.theatreId;

  const history = useHistory();

  const updatetheatreToDB = () => {
    if (theatreName.length === 0) {
      alert("Enter Name");
    } else if (theatreAddress.length === 0) {
      alert("Enter MobileNo");
    } else if (theatreCity.length === 0) {
      alert("Enter Password");
    } else {
      const data = new FormData();
      data.append("theatreName", theatreName);
      data.append("theatreAddress", theatreAddress);
      data.append("theatreCity", theatreCity);
      data.append("theatreId", id);

      axios.put(url + "/theatre/update/" + id, data).then((response) => {
        const result = response.data;
        if (result == "success") {
          alert("successfully added an theatre");
          history.push("/theatre");
        } else {
          alert("error while adding theatre");
        }
      });
    }
  };

  return (
    <div className="mainContainerDiv">
      <div Style="width:40%" className="formDiv">
        <div Style="display: flex" className="inputDiv">
          <label Style="width:50%">Theatre Name</label>
          <input
            type="text"
            value={theatreName}
            onChange={(e) => {
              setTheatreName(e.target.value);
            }}
            class="form-control"
            placeholder="Name"
          />
        </div>
        <div Style="display: flex" className="inputDiv">
          <label Style="width:50%">Theatre Address</label>
          <input
            type="text"
            value={theatreAddress}
            onChange={(e) => {
              setTheatreAddress(e.target.value);
            }}
            class="form-control"
            placeholder="Address"
          />
        </div>
        <div Style="display: flex" className="inputDiv">
          <label Style="width:50%">Theatre City</label>
          <input
            type="text"
            value={theatreCity}
            onChange={(e) => {
              setTheatreCity(e.target.value);
            }}
            class="form-control"
            placeholder="City"
          />
        </div>
        <div Style="display: flex" className="inputDiv">
          <label Style="width:50%">Theatre Id</label>
          <input
            type="text"
            value={Theatre.theatreId}
            onChange={(e) => {
              setTheatreCity(e.target.value);
            }}
            class="form-control"
            placeholder="Theatre Id"
          />
        </div>
        <div className="submitBtn" onClick={updatetheatreToDB}>
          Update
        </div>
      </div>
    </div>
  );
};

export default Updatetheatre;

// return (
//     <div className="centrealign container">
//       <table className="tablepadding">
//         <tr>
//           <td>
//             <input
//               type="text"
//               value={theatreName}
//               onChange={(e) => {
//                 setTheatreName(e.target.value);
//               }}
//               class="form-control"
//               placeholder="Name"
//             />
//             <br />
//             <input
//               type="text"
//               value={theatreAddress}
//               onChange={(e) => {
//                 setTheatreAddress(e.target.value);
//               }}
//               class="form-control"
//               placeholder="Address"
//             />
//             <br />
//             <input
//               type="text"
//               value={theatreCity}
//               onChange={(e) => {
//                 setTheatreCity(e.target.value);
//               }}
//               class="form-control"
//               placeholder="City"
//             />
//             <br />
//             <input
//               type="text"
//               value={Theatre.theatreId}
//               onChange={(e) => {
//                 setTheatreCity(e.target.value);
//               }}
//               class="form-control"
//               placeholder="City"
//             />
//             <br />

//             <button
//               className="btn btn-secondary btn-lg addbutton"
//               onClick={updatetheatreToDB}
//             >
//               Add
//             </button>
//           </td>
//         </tr>
//       </table>
//     </div>
//   );
