import { BrowserRouter, Link, Route, Switch } from "react-router-dom";
// import "../CSS/Theatre.css";
import { useEffect, useState } from "react";
import axios from "axios";
import { url } from "../../../Common/common";
import "../../ManagerComponent/css/List.css";
import { useHistory } from "react-router-dom";
import trashImg from "../../../Images/trash.png";
import updateImg from "../../../Images/updated.png";

const Theatre = () => {
  const history = useHistory();

  const [theatreName, setTheatreName] = useState("");
  const [theatreAddress, setTheatreAddress] = useState("");
  const [theatreCity, setTheatreCity] = useState("");
  const [changesMade, setChangesMade] = useState(false);
  const [theatre, setTheatre] = useState([]);

  useEffect(() => {
    getTheatreList();
  }, [changesMade]);

  //   useEffect(() => {
  //     getTheatreList();
  //   }, [theatreisdeleted]);

  const accesstheatre = (e) => {
    const id = e.target.id;
    axios.get(url + "/theatre/getTheatre/" + id).then((response) => {
      const result = response.data;

      if (result != null) {
        alert("successfully access theatre");
        localStorage.setItem("theatre", JSON.stringify(result));
        history.push("/updatetheatre");
      } else {
        alert("error while accessing  theatre");
      }
    });
  };

  const deletetheatre = (e) => {
    const id = e.target.id;
    axios.delete(url + "/theatre/" + id).then((response) => {
      const result = response.data;
      if (result == "success") {
        {
          changesMade ? setChangesMade(false) : setChangesMade(true);
        }
        alert("successfully deleted an manager");
      } else {
        alert("error while deleting  manager");
      }
    });
  };
  const getTheatreList = () => {
    axios.get(url + "/theatre/").then((response) => {
      const result = response.data;
      setTheatre(result);
    });
  };

  const addtheatreToDB = () => {
    if (theatreName.length === 0) {
      alert("Enter Name");
    } else if (theatreAddress.length === 0) {
      alert("Enter Address");
    } else if (theatreCity.length === 0) {
      alert("Enter City");
    } else {
      const data = new FormData();

      data.append("theatreName", theatreName);
      data.append("theatreAddress", theatreAddress);
      data.append("theatreCity", theatreCity);

      axios.post(url + "/theatre/add", data).then((response) => {
        const result = response.data;
        if (result == "success") {
          {
            changesMade ? setChangesMade(false) : setChangesMade(true);
          }
          alert("successfully added an theatre");
        } else {
          alert("error while adding theatre");
        }
      });
    }
  };

  return (
    <div className="mainContainerDiv">
      <div className="formDiv">
        <div className="inputDiv">
          <input
            type="text"
            onChange={(e) => {
              setTheatreName(e.target.value);
            }}
            class="form-control"
            placeholder="Enter Theatre Name"
          />
        </div>
        <div className="inputDiv">
          <input
            type="text"
            onChange={(e) => {
              setTheatreAddress(e.target.value);
            }}
            class="form-control"
            placeholder="Enter Theatre Address"
          />
        </div>
        <div className="inputDiv">
          <input
            type="text"
            onChange={(e) => {
              setTheatreCity(e.target.value);
            }}
            class="form-control"
            placeholder="Enter Theatre City"
          />
        </div>
        <div onClick={addtheatreToDB} className="submitBtn">
          Submit
        </div>
      </div>
      <div className="fetchedDataDiv">
        <div className="titleDiv">
          <div className="movieIddiv">City</div>
          <div className="movieNamediv">Theatre Id</div>
          {/* <div className="movieNamediv">Theatre Name</div> */}
          <div className="deleteBtndiv">Delete</div>
          <div className="updateBtndiv">Update</div>
        </div>
        <div className="itemList">
          {theatre.map((thtr) => {
            return (
              <div className="itemRow">
                <div className="movieIddiv">{thtr.theatreCity}</div>
                <div className="movieNamediv">{thtr.theatreId}</div>
                {/* <div className="movieNamediv">{thtr.theatreName}</div> */}
                <div className="deleteBtndiv">
                  <img
                    className="deleteImg"
                    id={thtr.theatreId}
                    onClick={deletetheatre}
                    src={trashImg}
                    alt="delete"
                  />
                </div>
                <div className="updateBtndiv">
                  <img
                    className="updateImg"
                    id={thtr.theatreId}
                    onClick={accesstheatre}
                    src={updateImg}
                    alt="update"
                  />
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default Theatre;

// return (
//     <div className="container table1">
//       <table>
//         <tr>
//           <td>
//             <div class="form-floating mb-6">
//               <table className="tablepadding">
//                 <tr>
//                   <td>
//                     <input
//                       type="text"
//                       onChange={(e) => {
//                         setTheatreName(e.target.value);
//                       }}
//                       class="form-control"
//                       placeholder="Enter Theatre Name"
//                     />
//                     <br />
//                     <input
//                       type="text"
//                       onChange={(e) => {
//                         setTheatreAddress(e.target.value);
//                       }}
//                       class="form-control"
//                       placeholder="Enter Theatre Address"
//                     />
//                     <br />
//                     <input
//                       type="text"
//                       onChange={(e) => {
//                         setTheatreCity(e.target.value);
//                       }}
//                       class="form-control"
//                       placeholder="Enter Theatre City"
//                     />
//                     <br />

//                     <button
//                       className="btn btn-secondary btn-lg addbutton"
//                       onClick={addtheatreToDB}
//                     >
//                       Add
//                     </button>
//                   </td>
//                 </tr>
//               </table>
//             </div>
//           </td>
//           <td>
//             <div className="table2 ">
//               <table>
//                 <thead>
//                   <tr>
//                     <th className="tablecolumn">City</th>
//                     <th className="tablecolumn">Theatre Id</th>
//                     <th className="tablecolumn">Delete</th>
//                     <th className="tablecolumn">Update</th>
//                   </tr>
//                 </thead>
//                 <tbody>
//                   {theatre.map((thtr) => {
//                     return (
//                       <tr>
//                         <td> {thtr.theatreCity} </td>
//                         <td>{thtr.theatreId} </td>
//                         <td>
//                           {" "}
//                           <button
//                             className="btn btn-secondary"
//                             id={thtr.theatreId}
//                             onClick={deletetheatre}
//                           >
//                             delete
//                           </button>
//                         </td>
//                         <td>
//                           {" "}
//                           <button
//                             className="btn btn-secondary"
//                             id={thtr.theatreId}
//                             onClick={accesstheatre}
//                           >
//                             Update
//                           </button>
//                         </td>
//                       </tr>
//                     );
//                   })}
//                 </tbody>
//               </table>
//             </div>
//           </td>
//         </tr>
//       </table>
//     </div>
//   );
// };
