import "./css/NavBar.css";
import {
  BrowserRouter,
  Link,
  Route,
  Switch,
  NavLink,
  Redirect,
} from "react-router-dom";
import Movie from "../../Component/MovieComponent/Movie-copy";
import Theatre from "../../Component/TheatreComponent/Theatre";
import { useMemo, useState, useContext } from "react";
import EditProfile from "../../CommonPages/SignIn-Up-Edit/EditProfile";
import userLogo from "../../Images/userBar.png";
import MovieInfo from "../MovieComponent/MovieInfo";
import TheatreInfo from "../TheatreComponent/TheatreInfo";
import AuthContext from "../../Common/auth-context";
import TicketBooked from "../ShowComponent/TicketBooked";
import ShowsByTheatre from "../ShowComponent/ShowsByTheatre";
import TheatreToMovieInfo from "../TheatreComponent/TheatreToMovieInfo";
import { useHistory } from "react-router";
import Signin from "../../CommonPages/SignIn-Up-Edit/Signin";
import TheatreItem from "../TheatreComponent/TheatreItem";
// import MovieInfo from "../CommonPages/MovieInfo";

const NavBar = () => {
  const { user, setUser } = useContext(AuthContext);
  const [searchValue, setSearchValue] = useState("");
  const history = useHistory();
  const logOutHandler = () => {
    localStorage.removeItem("myUser");
    setUser(null);
  };

  const somPart = () => {
    return (
      <ul
        className="dropdown-menu dropDownContent dropdown-menu-end "
        aria-labelledby="navbarDropdownMenuLink"
      >
        <li>
          <NavLink
            className="nav-link dropdown-item myDropdown"
            to="/editprofile"
          >
            <b>Edit Profile</b>
          </NavLink>
        </li>
        <li>
          <div
            onClick={logOutHandler}
            className="nav-link dropdown-item myDropdown"
            to="/signout"
          >
            <b>Sign Out</b>
          </div>
        </li>
      </ul>
    );
  };

  // const otherPart = () => {
  //   return (
  //     <ul
  //       className="dropDownContent dropdown-menu dropdown-menu-end "
  //       aria-labelledby="navbarDropdownMenuLink"
  //     >
  //       {/* <li>
  //         <NavLink className="nav-link dropdown-item myDropdown" to="/signin">
  //           <b>Sign in</b>
  //         </NavLink>
  //       </li>
  //       <li>
  //         <NavLink className="nav-link dropdown-item myDropdown" to="/signup">
  //           <b>Register</b>
  //         </NavLink>
  //       </li> */}
  //     </ul>
  //   );
  // };

  const profileLogoLoader = () => {
    return (
      <div className="profileLogoContainer">
        <img className="profileLogo" src={userLogo} />
      </div>
    );
  };

  return (
    <div>
      <BrowserRouter>
        <nav className="navbar navbar-expand-lg navbar-dark navContainer">
          <div className="container-fluid">
            <span className="logoFont">
              <NavLink className="navbar-brand" to="/">
                Movie Lovers
              </NavLink>
            </span>

            <div className="searchContainer">
                <input
                  className="form-control searchBox"
                  type="text"
                  placeholder="Search"
                  aria-label="Search"
                  onKeyUp = {(e)=> {setSearchValue(e.target.value)}}
                />
            </div>

            <div>
              <button
                className="navbar-toggler"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#navbarNav"
                aria-controls="navbarNav"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="navbar-toggler-icon"></span>
              </button>
              <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav">
                  <li className="nav-item">
                    <NavLink className="nav-link nav myLink" to="/theatre">
                      <b>Theatre</b>
                    </NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link nav myLink" to="/">
                      <b>Movie</b>
                    </NavLink>
                  </li>

                  <li className="nav-item dropDownContainer dropdown">
                    {" "}
                    {/*dropDownContainer */}
                    <a
                      className="nav-link {/*dropdown-toggle*/} "
                      href="#"
                      id="navbarDropdownMenuLink"
                      role="button"
                      data-bs-toggle="dropdown"
                      aria-expanded="false"
                    >
                      {user != null && (
                        <div className="userName">
                          Hi {user.userName.split(" ")[0]} !
                        </div>
                      )}
                    </a>
                    {user != null && somPart()}
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </nav>
        <div>
          <Switch>
            <Route path="/signin" component={Signin} />
            <Route path="/editProfile" component={EditProfile} />
            <Route path="/movie-info" component={MovieInfo} />
            <Route path="/theatre-info" component={TheatreInfo} />
            <Route path="/showBytheatre" component={ShowsByTheatre} />
            <Route path="/theatreToMovieInfo" component={TheatreToMovieInfo} />
            <Route path="/ticket-booked" component={TicketBooked} />
            <Route path="/theatre" component={()=><Theatre word ={searchValue} />} />
            <Route exact path="/" component={()=><Movie word ={searchValue} />} />
          </Switch>
        </div>
      </BrowserRouter>
    </div>
  );
};

export default NavBar;
