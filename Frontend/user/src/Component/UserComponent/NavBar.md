import "./css/NavBar.css";
import { BrowserRouter, Link, Route, Switch, NavLink } from "react-router-dom";
import Movie from "../../Component/MovieComponent/Movie-copy";
import Theatre from "../../Component/TheatreComponent/Theatre";
import { useMemo, useState } from "react";
import EditProfile from "../../CommonPages/SignIn-Up-Edit/EditProfile";
import userLogo from "../../Images/userBar.png";
import MovieInfo from "../MovieComponent/MovieInfo";
import TheatreInfo from "../TheatreComponent/TheatreInfo";
import AuthContext from "../../Common/auth-context";
// import MovieInfo from "../CommonPages/MovieInfo";

const NavBar = () => {
  let signedInUser = JSON.parse(localStorage.getItem("myUser"));
  const [user, setUser] = useState(signedInUser);
  // const value = useMemo(() => ({ user, setUser }), [user, setUser]);

  const logOutHandler = () => {
    localStorage.removeItem("myUser");
    setUser(null);
  };

  const somPart = () => {
    return (
      <ul
        className="dropdown-menu dropDownContent dropdown-menu-end "
        aria-labelledby="navbarDropdownMenuLink"
      >
        <li>
          <NavLink
            className="nav-link dropdown-item myDropdown"
            to="/editprofile"
          >
            <b>Edit Profile</b>
          </NavLink>
        </li>
        <li>
          <div
            onClick={logOutHandler}
            className="nav-link dropdown-item myDropdown"
            to="/signout"
          >
            <b>Sign Out</b>
          </div>
        </li>
      </ul>
    );
  };

  const otherPart = () => {
    return (
      <ul
        className="dropDownContent dropdown-menu dropdown-menu-end "
        aria-labelledby="navbarDropdownMenuLink"
      >
        <li>
          <NavLink className="nav-link dropdown-item myDropdown" to="/signin">
            <b>Sign in</b>
          </NavLink>
        </li>
        <li>
          <NavLink className="nav-link dropdown-item myDropdown" to="/signup">
            <b>Register</b>
          </NavLink>
        </li>
      </ul>
    );
  };

  const profileLogoLoader = () => {
    return (
      <div className="profileLogoContainer">
        <img className="profileLogo" src={userLogo} />
      </div>
    );
  };

  return (
    <div className="mainContainer">
      <BrowserRouter>
        <nav className="navbar navbar-expand-lg navbar-light navContainer">
          <div className="container-fluid">
            <span className="logoFont">
              <NavLink className="navbar-brand" to="/">
                Movie Lovers
              </NavLink>
            </span>

            <div className="container-fluid navbar navbar-dark searchContainer">
              {" "}
              {/* search bar  */}
              <form className="d-flex">
                <input
                  className="form-control searchBox"
                  type="search"
                  placeholder="Search"
                  aria-label="Search"
                />
                <button className=" searchBtn" type="submit">
                  <b>Search</b>
                </button>
              </form>
            </div>

            <div className="div">
              {/*nav Bar toggler */}
              <button
                className="navbar-toggler"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#navbarNav"
                aria-controls="navbarNav"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="navbar-toggler-icon"></span>
              </button>
              <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav">
                  {/* <li className="nav-item">
                    <NavLink className="nav-link myLink" to="/employeeHome">
                      <b>Employee</b>
                    </NavLink>
                  </li> */}

                  <li className="nav-item">
                    <NavLink className="nav-link nav myLink" to="/theatre">
                      <b>Theatre</b>
                    </NavLink>
                  </li>
                  <li>
                    <NavLink className="nav-link nav myLink" to="/">
                      <b>Movie</b>
                    </NavLink>
                  </li>

                  <li className="nav-item dropDownContainer dropdown">
                    {" "}
                    {/*dropDownContainer */}
                    <a
                      className="nav-link {/*dropdown-toggle*/} "
                      href="#"
                      id="navbarDropdownMenuLink"
                      role="button"
                      data-bs-toggle="dropdown"
                      aria-expanded="false"
                    >
                      {user != null ? (
                        <div className="userName">Hi {user.userName} !</div>
                      ) : (
                        profileLogoLoader()
                      )}
                    </a>
                    {user != null ? somPart() : otherPart()}
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </nav>
        <div>
          <Switch>
            {/*Switch and Routes */}
            {/* <Route  path="/employeeHome" component={EmployeeHome} /> */}
            <AuthContext.Provider value={{ user, setUser }}>
              <Route path="/theatre" component={Theatre} />
              {/* <Route path="/signout" component={SignOut} /> */}
              <Route path="/signin" component={Signin} />
              <Route path="/signup" component={Signup} />
              <Route path="/editProfile" component={EditProfile} />
              <Route exact path="/" component={Movie} />
            </AuthContext.Provider>
          </Switch>
        </div>
      </BrowserRouter>
    </div>
  );
};

export default NavBar;
