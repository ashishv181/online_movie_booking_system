import { BrowserRouter, Link, Route, Switch } from "react-router-dom";
import "./Headerfooter.css";
import { useEffect, useState } from "react";
import axios from "axios";
import { url } from "../../../Common/common";
import "../css/List.css";
import { useHistory } from "react-router-dom";

const ShowUpdate = () => {
  const show = JSON.parse(localStorage.getItem("show"));
  //console.log(JSON.parse(show))
  const theatreId = JSON.parse(localStorage.getItem("myUser")).theatre
    .theatreId;
  const [showNumber, setshowNumber] = useState(show.showNumber);
  const [showStartTime, setshowStartTime] = useState(show.showStartTime);
  const [showEndTime, setshowEndTime] = useState(show.showEndTime);
  const [noOfSeatsLeft, setnoOfSeatsLeft] = useState(show.noOfSeatsLeft);
  const [showRevenue, setshowRevenue] = useState(show.showRevenue);
  const [theatre, settheatre] = useState(show.theatre_id);
  const [screen, setscreen] = useState(show.screen_id);
  const [movie, setmovie] = useState(show.movie.movieId);
  const id = show.showId;

  const history = useHistory();

  const addshowToDB = () => {
    if (!showNumber) {
      alert("Enter Show number");
    } else if (!showStartTime) {
      alert("Enter Show start time");
    } else if (!showEndTime) {
      alert("Enter Show end time");
    } else if (!noOfSeatsLeft) {
      alert("Enter no of seats left");
    } else if (!movie) {
      alert("Enter movie ID");
    } else {
      // when a file needs to be uploaded use FormData
      const data = new FormData();

      // add the data
      data.append("showId", id);
      data.append("showNumber", showNumber);
      data.append("showStartTime", showStartTime);
      data.append("showEndTime", showEndTime);
      data.append("noOfSeatsLeft", noOfSeatsLeft);
      data.append("theatre", theatreId);
      data.append("movie", movie);

      // send the data to the API
      axios.put(url + "/show/", data).then((response) => {
        console.log("put method call");
        const result = response.data;
        if (result.status === "success") {
          alert("successfully added an show");
          history.push("/show");
        } else {
          alert("error while adding show");
        }
      });
    }
  };

  return (
    <div className="mainContainerDiv">
      <div Style="width: 40%" className="formDiv">
        <div Style="display: flex" className="inputDiv">
          <label Style="width:50%">Show Number</label>
          <input
            type="number"
            value={showNumber}
            onChange={(e) => {
              setshowNumber(e.target.value);
            }}
            class="form-control"
            placeholder="Show number"
          />
        </div>
        <div Style="display: flex" className="inputDiv">
          <label Style="width:50%">Show Start</label>

          <input
            type="datetime-local"
            value={showStartTime}
            onChange={(e) => {
              setshowStartTime(e.target.value);
            }}
            class="form-control"
            placeholder="Show start time"
          />
        </div>
        <div Style="display: flex" className="inputDiv">
          <label Style="width:50%">Show End</label>

          <input
            type="datetime-local"
            value={showEndTime}
            onChange={(e) => {
              setshowEndTime(e.target.value);
            }}
            class="form-control"
            placeholder="Show end time"
          />
        </div>
        <div Style="display: flex" className="inputDiv">
          <label Style="width:50%">Seats left</label>

          <input
            type="number"
            value={noOfSeatsLeft}
            onChange={(e) => {
              setnoOfSeatsLeft(e.target.value);
            }}
            class="form-control"
            placeholder="no of seats left"
          />
        </div>

        <div Style="display: flex" className="inputDiv">
          <label Style="width:50%">Movie Id</label>

          <input
            type="number"
            value={movie}
            onChange={(e) => {
              setmovie(e.target.value);
            }}
            class="form-control"
            placeholder="Movie Id"
          />
        </div>
        <div className="submitBtn" onClick={addshowToDB}>
          Update
        </div>
      </div>
    </div>
  );
};

export default ShowUpdate;

// return (
//   <div className="centrealign container">
//     <table className="tablepadding">
//       <tr>
//         <td>
//           <input
//             type="number"
//             value={showNumber}
//             onChange={(e) => {
//               setshowNumber(e.target.value);
//             }}
//             class="form-control"
//             placeholder="Show number"
//           />
//           <br />
//           <input
//             type="datetime-local"
//             value={showStartTime}
//             onChange={(e) => {
//               setshowStartTime(e.target.value);
//             }}
//             class="form-control"
//             placeholder="Show start time"
//           />
//           <br />
//           <input
//             type="datetime-local"
//             value={showEndTime}
//             onChange={(e) => {
//               setshowEndTime(e.target.value);
//             }}
//             class="form-control"
//             placeholder="Show end time"
//           />
//           <br />
//           <input
//             type="number"
//             value={noOfSeatsLeft}
//             onChange={(e) => {
//               setnoOfSeatsLeft(e.target.value);
//             }}
//             class="form-control"
//             placeholder="no of seats left"
//           />
//           <br />
//           <input
//             type="number"
//             value={showRevenue}
//             onChange={(e) => {
//               setshowRevenue(e.target.value);
//             }}
//             class="form-control"
//             placeholder="Show revenue"
//           />
//           <br />
//           <input
//             type="number"
//             value={theatre}
//             onChange={(e) => {
//               settheatre(e.target.value);
//             }}
//             class="form-control"
//             placeholder="Theatre Id"
//           />
//           <br />
//           <input
//             type="number"
//             value={screen}
//             onChange={(e) => {
//               setscreen(e.target.value);
//             }}
//             class="form-control"
//             placeholder="screen Id "
//           />
//           <br />
//           <input
//             type="number"
//             value={movie}
//             onChange={(e) => {
//               setmovie(e.target.value);
//             }}
//             class="form-control"
//             placeholder="Movie Id"
//           />
//           <br />
//           <button
//             className="btn btn-secondary btn-lg addbutton"
//             onClick={addshowToDB}
//           >
//             Add
//           </button>
//         </td>
//       </tr>
//     </table>
//   </div>
// );
