import { BrowserRouter, Link, Route, Switch } from "react-router-dom";
// import "./Headerfooter.css";
import { useEffect, useState } from "react";
import axios from "axios";
import { url } from "../../../Common/common";
import Update from "./Update";
import { useHistory } from "react-router-dom";

const Manager = () => {
const history = useHistory();

const [managerName, setManagerName] = useState("");
const [managerMobile, setManagerMobile] = useState("");
const [managerEmail, setManagerEmail] = useState("");
const [managerPassword, setManagerPassword] = useState("");
const [managerDob, setManagerDob] = useState("");
const [theatre, setTheatre] = useState(0);

const [manager, setManager] = useState([]);
const [managerisdeleted, setManagerisdeleted] = useState(false);

useEffect(() => {
getaAllManager();
}, [managerisdeleted]);

useEffect(() => {
getaAllManager();
}, [managerisdeleted]);

const accessmanager = (e) => {
const id = e.target.id;
axios.get(url + "/manager/" + id).then((response) => {
const result = response.data;

      if (result.status !== null) {
        alert("successfully access manager");

        localStorage.setItem("manager", JSON.stringify(result.status));
        history.push("/update");
      } else {
        alert("error while accessing  manager");
      }
    });

};

const deletemanager = (e) => {
const id = e.target.id;
axios.delete(url + "/manager/" + id).then((response) => {
const result = response.data;
if (result.status === "success") {
setManagerisdeleted(true);
alert("successfully deleted an manager");
} else {
alert("error while deleting manager");
}
});
};
const getaAllManager = () => {
axios.get(url + "/manager/getmanager").then((response) => {
const result = response.data;
setManager(result);
});
};

const addManagerToDB = () => {
if (managerName.length === 0) {
alert("Enter Name");
} else if (managerMobile.length === 0) {
alert("Enter MobileNo");
} else if (managerEmail.length === 0) {
alert("Enter Email");
} else if (managerPassword.length === 0) {
alert("Enter Password");
} else if (managerDob.length === 0) {
alert("Enter DOB");
} else if (theatre.length === 0) {
alert("Enter TheatreId");
} else {
// when a file needs to be uploaded use FormData
const data = new FormData();

      // add the data
      data.append("managerName", managerName);
      data.append("managerMobile", managerMobile);
      data.append("managerEmail", managerEmail);
      data.append("managerPassword", managerPassword);
      data.append("managerDob", managerDob);
      data.append("theatre", theatre);

      // send the data to the API
      axios.post(url + "/manager", data).then((response) => {
        const result = response.data;
        if (result.status === "success") {
          alert("successfully added an manager");
        } else {
          alert("error while adding manager");
        }
      });
    }

};

return (
<div className="container table1">
<table>
<tr>
<td>
<div class="form-floating mb-6">
<table className="tablepadding">
<tr>
<td>
<input
type="text"
onChange={(e) => {
setManagerName(e.target.value);
}}
class="form-control"
placeholder="Name"
/>
<br />
<input
type="text"
onChange={(e) => {
setManagerMobile(e.target.value);
}}
class="form-control"
placeholder="Mobile No"
/>
<br />
<input
type="Email"
onChange={(e) => {
setManagerEmail(e.target.value);
}}
class="form-control"
placeholder="Email"
/>
<br />
<input
type="Password"
onChange={(e) => {
setManagerPassword(e.target.value);
}}
class="form-control"
placeholder="Password"
/>
<br />
<input
type="date"
onChange={(e) => {
setManagerDob(e.target.value);
}}
class="form-control"
placeholder="Date of Birth"
/>
<br />
<input
type="number"
onChange={(e) => {
setTheatre(e.target.value);
}}
class="form-control"
placeholder="Theatre Id"
/>
<br />
<button
                      className="btn btn-secondary btn-lg addbutton"
                      onClick={addManagerToDB}
                    >
Add
</button>
</td>
</tr>
</table>
</div>
</td>
<td>
<div className="table2 ">
<table>
<thead>
<tr>
<th className="tablecolumn">Show Id</th>
<th className="tablecolumn">Manager Name</th>
<th className="tablecolumn">Delete</th>
<th className="tablecolumn">Update</th>
</tr>
</thead>
<tbody>
{manager.map((man) => {
return (
<tr>
<td> {man.managerId} </td>
<td>{man.managerName} </td>
<td>
{" "}
<button
                            className="btn btn-secondary"
                            id={man.managerId}
                            onClick={deletemanager}
                          >
delete
</button>
</td>
<td>
{" "}
<button
                            className="btn btn-secondary"
                            id={man.managerId}
                            onClick={accessmanager}
                          >
Update
</button>
</td>
</tr>
);
})}
</tbody>
</table>
</div>
</td>
</tr>
</table>
</div>
);
};

export default Manager;

{
/\* <div className="verticle">

    <BrowserRouter>

    <div className="div">

    <Link className="btn btn-primary btn-sm" to="/addmanager">Add Manager</Link>
    </div>
    <br></br>
    <div className="div">
    <Link className="btn btn-primary btn-sm" to="/deletemanager">Delete Manager</Link>
    </div>
    <br></br>
    <div className="div">
    <Link className="btn btn-primary btn-sm" to="/updateemanager">Update Manager</Link>
    </div>


    <Switch>
    <Route path="/addmanager" component={Trial} />
    <Route path="/deletemanager" component={Trial} />
    <Route path="/updateemanager" component={Trial} />

    </Switch>
    </BrowserRouter>

  </div> */
}
