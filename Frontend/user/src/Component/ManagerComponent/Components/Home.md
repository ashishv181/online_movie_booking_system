import HeaderPage from "./Header"
import {  BrowserRouter, Link, Route, Switch } from 'react-router-dom';
import Trial from "./Trial";


const Home = () => {
    return (
        <div>
       <BrowserRouter>
        <a href="/trial">Trial</a>
        <Switch>
        <Route path="/trial" component={Trial} />
            
        </Switch>
        </BrowserRouter>
    
     
     </div>
    )
  }
  
  export default Home