import { BrowserRouter, Link, Route, Switch } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios";
import { url } from "../../../Common/common";
import "../css/List.css";
import trashImg from "../../../Images/trash.png";
import updateImg from "../../../Images/updated.png";
import ShowUpdate from "./ShowUpdate";
import { useHistory } from "react-router-dom";



const Show = () => {
  const history = useHistory();
  const theatreId = JSON.parse(localStorage.getItem("myUser")).theatre
    .theatreId;
  const [showNumber, setshowNumber] = useState("");
  const [showStartTime, setshowStartTime] = useState("");
  const [showEndTime, setshowEndTime] = useState("");
  const [noOfSeatsLeft, setnoOfSeatsLeft] = useState("");
  const [movie, setmovie] = useState(0);
  const [show, setshow] = useState([]);
  const [showisdeleted, setShowisdeleted] = useState(false);
  const [changesMade, setChangesMade] = useState(false);

  useEffect(() => {
    getaAllShow();
  }, [changesMade]);

  const accessShow = (e) => {
    const id = e.target.id;
    axios.get(url + "/show/" + id).then((response) => {
      const result = response.data;

      if (result.status !== null) {
        alert("successfully access Show");

        localStorage.setItem("show", JSON.stringify(result.status));
        history.push("/ShowUpdate");
      } else {
        alert("error while accessing  Show");
      }
    });
  };

  const deleteShow = (e) => {
    const id = e.target.id;
    axios.delete(url + "/show/" + id).then((response) => {
      const result = response.data;
      if (result.status == "success") {
        {
          changesMade ? setChangesMade(false) : setChangesMade(true);
        }
        alert("successfully deleted an Show");
      } else {
        alert("error while deleting  Show");
      }
    });
  };
  const getaAllShow = () => {
    axios.get(url + "/show/getShow").then((response) => {
      const result = response.data;
      setshow(result);
    });
  };

  const addShowToDB = () => {
    if (!showNumber) {
      alert("Enter Show number");
    } else if (!showStartTime) {
      alert("Enter Show start time");
    } else if (!showEndTime) {
      alert("Enter Show end time");
    } else if (!noOfSeatsLeft) {
      alert("Enter no of seats left");
    } else if (!movie) {
      alert("Enter screen ID");
    } else {
      const data = new FormData();
      data.append("showNumber", showNumber);
      data.append("showStartTime", showStartTime);
      data.append("showEndTime", showEndTime);
      data.append("noOfSeatsLeft", noOfSeatsLeft);
      data.append("theatre", theatreId);
      data.append("movie", movie);

      axios.post(url + "/show", data).then((response) => {
        const result = response.data;
        if (result.status == "success") {
          {
            changesMade ? setChangesMade(false) : setChangesMade(true);
          }
          alert("successfully added an Show");
        } else {
          alert("error while adding Show");
        }
      });
    }
  };

  return (
    <div className="mainContainerDiv">
      <div className="formDiv">
        <div className="inputDiv">
          <input
            type="number"
            onChange={(e) => {
              setshowNumber(e.target.value);
            }}
            class="form-control"
            placeholder="Show number"
          />
        </div>
        <div className="inputDiv">
          <input
            type="datetime-local"
            onChange={(e) => {
              setshowStartTime(e.target.value);
            }}
            class="form-control"
            placeholder="Show start time"
          />
        </div>
        <div className="inputDiv">
          <input
            type="datetime-local"
            onChange={(e) => {
              setshowEndTime(e.target.value);
            }}
            class="form-control"
            placeholder="Show end time"
          />
        </div>
        <div className="inputDiv">
          <input
            type="number"
            onChange={(e) => {
              setnoOfSeatsLeft(e.target.value);
            }}
            class="form-control"
            placeholder="no of seats left"
          />
        </div>

        <div className="inputDiv">
          <input
            type="number"
            onChange={(e) => {
              setmovie(e.target.value);
            }}
            class="form-control"
            placeholder="Movie Id"
          />
        </div>
        <div onClick={addShowToDB} className="submitBtn">
          Submit
        </div>
      </div>
      <div className="fetchedDataDiv">
        <div className="titleDiv">
          <div className="movieIddiv">Show Id</div>
          <div className="movieNamediv">Show Number</div>
          <div className="deleteBtndiv">Delete</div>
          <div className="updateBtndiv">Update</div>
        </div>
        <div className="itemList">
          {show.map((sh) => {
            return (
              <div className="itemRow">
                <div className="movieIddiv">{sh.showId}</div>
                <div className="movieNamediv">{sh.showNumber}</div>
                <div className="deleteBtndiv">
                  <img
                    className="deleteImg"
                    id={sh.showId}
                    onClick={deleteShow}
                    src={trashImg}
                    alt="delete"
                  />
                </div>
                <div className="updateBtndiv">
                  <img
                    className="updateImg"
                    id={sh.showId}
                    onClick={accessShow}
                    src={updateImg}
                    alt="update"
                  />
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default Show;
