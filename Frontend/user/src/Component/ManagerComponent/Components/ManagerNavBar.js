import { BrowserRouter, Link, Route, Switch, NavLink } from "react-router-dom";
// import "../css/Headerfooter.css";
import { useState } from "react";
// import Manager from "./Manager";
import Employee from "./Employee";
import Show from "./Show";
import Movie from "./Movie";
// import Update from "./Update";
import MovieUpdate from "./MovieUpdate";
import ShowUpdate from "./ShowUpdate";
import EmployeeUpdate from "./EmployeeUpdate";
import AuthContext from "../../../Common/auth-context";
import "../../UserComponent/css/NavBar.css";
import EditProfile from "../../../CommonPages/SignIn-Up-Edit/EditProfile";
import { useContext } from "react";

const HeaderPage = () => {
  const { user, setUser } = useContext(AuthContext);
  const [firstName, setName] = useState(user.managerName.split(" ")[0]);

  const logOutHandler = () => {
    localStorage.removeItem("myUser");
    localStorage.removeItem("role");
    localStorage.removeItem("employee");
    localStorage.removeItem("show");
    localStorage.removeItem("movie");
    setUser(null);
  };

  return (
    <div>
      <BrowserRouter>
        <nav className="navbar navbar-expand-lg navbar-dark navContainer">
          <div className="container-fluid">
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarNav"
              aria-controls="navbarNav"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
              <ul className="navbar-nav">
                <li className="nav-item">
                  <Link className="nav-link" to="/movie">
                    <button className="btn btn-dark">Movie</button>
                  </Link>
                </li>

                <li className="nav-item">
                  <Link className="nav-link" to="/employee">
                    <button className="btn btn-dark"> Employee</button>
                  </Link>
                </li>

                <li className="nav-item">
                  <Link className="nav-link" to="/show">
                    <button className="btn btn-dark"> Show</button>
                  </Link>
                </li>

                <div className="flex">
                  <li className="nav-item dropDownContainer dropdown">
                    <a
                      className="nav-link {/*dropdown-toggle*/} "
                      href="#"
                      id="navbarDropdownMenuLink"
                      role="button"
                      data-bs-toggle="dropdown"
                      aria-expanded="false"
                    >
                      {user != null && (
                        <div className="userName">Hi {firstName}!</div>
                      )}
                    </a>
                    <ul
                      className="dropdown-menu dropDownContent dropdown-menu-end "
                      aria-labelledby="navbarDropdownMenuLink"
                    >
                      <li>
                        <NavLink
                          className="nav-link dropdown-item myDropdown"
                          to="/editprofile"
                        >
                          <b>Edit Profile</b>
                        </NavLink>
                      </li>
                      <li>
                        <div
                          onClick={logOutHandler}
                          className="nav-link dropdown-item myDropdown"
                        >
                          <b>Sign Out</b>
                        </div>
                      </li>
                    </ul>
                  </li>
                </div>
              </ul>
            </div>
          </div>
        </nav>

        <Switch>
          {/* <Route path="/manager" component={Manager} /> */}
          {/* <Route path="/update" component={Update} /> */}

          <Route path="/movie" component={Movie} />
          <Route path="/MovieUpdate" component={MovieUpdate} />
          <Route path="/employee" component={Employee} />
          <Route path="/EmployeeUpdate" component={EmployeeUpdate} />
          <Route path="/show" component={Show} />
          <Route path="/ShowUpdate" component={ShowUpdate} />
          <Route path="/editprofile" component={EditProfile} />
        </Switch>
      </BrowserRouter>
    </div>
  );
};

export default HeaderPage;
