import { BrowserRouter, Link, Route, Switch } from "react-router-dom";
import "./Headerfooter.css";
import { useEffect, useState } from "react";
import axios from "axios";
import { url } from "../../../Common/common";
import "../css/List.css";

import { useHistory } from "react-router-dom";

const EmployeeUpdate = () => {
  const Employee = JSON.parse(localStorage.getItem("employee"));
  const [employeeName, setemployeeName] = useState(Employee.employeeName);
  const [employeeMobile, setemployeeMobile] = useState(Employee.employeeMoblie);
  const [employeeEmail, setemployeeEmail] = useState(Employee.employeeEmail);
  const [employeePassword, setemployeePassword] = useState(
    Employee.employeePassword
  );
  const [employeeDob, setemployeeDob] = useState(Employee.employeeDob);
  const id = Employee.employeeId;
  const theatreId = JSON.parse(localStorage.getItem("myUser")).theatre
    .theatreId;

  console.log(employeeMobile);
  const history = useHistory();

  const addEmployeeToDB = () => {
    if (employeeName.length === 0) {
      alert("Enter Name");
    } else if (employeeMobile.length === 0) {
      alert("Enter MobileNo");
    } else if (employeeEmail.length === 0) {
      alert("Enter Email");
    } else if (employeePassword.length === 0) {
      alert("Enter Password");
    } else if (employeeDob.length === 0) {
      alert("Enter DOB");
    } else {
      const data = new FormData();

      data.append("employeeId", Employee.employeeId);
      data.append("employeeName", employeeName);
      data.append("employeeMobile", employeeMobile);
      data.append("employeeEmail", employeeEmail);
      data.append("employeePassword", employeePassword);
      data.append("employeeDob", employeeDob);
      data.append("theatre", theatreId);

      axios.put(url + "/employee/update", data).then((response) => {
        const result = response.data;
        if (result == "success") {
          alert("successfully updated an Employee");
          history.push("/employee");
        } else {
          alert("error while updating Employee");
        }
      });
    }
  };

  return (
    <div Style="width: 40%" className="mainContainerDiv">
      <div Style="width: 50%" className="formDiv">
        <div className="inputDiv">
          <input
            type="text"
            value={employeeName}
            onChange={(e) => {
              setemployeeName(e.target.value);
            }}
            class="form-control"
            placeholder="Name"
          />
        </div>
        <div className="inputDiv">
          <input
            type="text"
            value={employeeMobile}
            onChange={(e) => {
              setemployeeMobile(e.target.value);
            }}
            class="form-control"
            placeholder="Mobile No"
          />
        </div>
        <div className="inputDiv">
          <input
            type="Email"
            value={employeeEmail}
            onChange={(e) => {
              setemployeeEmail(e.target.value);
            }}
            class="form-control"
            placeholder="Email"
          />
        </div>
        <div className="inputDiv">
          <input
            type="Password"
            value={employeePassword}
            onChange={(e) => {
              setemployeePassword(e.target.value);
            }}
            class="form-control"
            placeholder="Password"
          />
        </div>
        <div className="inputDiv">
          <input
            type="date"
            value={employeeDob}
            onChange={(e) => {
              setemployeeDob(e.target.value);
            }}
            class="form-control"
            placeholder="Date of Birth"
          />
        </div>
        <div className="submitBtn" onClick={addEmployeeToDB}>
          Update
        </div>
      </div>
    </div>
  );
};
export default EmployeeUpdate;

// (
//     <div className="centrealign container">
//       <table className="tablepadding">
//         <tr>
//           <td>
//             <input
//               type="text"
//               value={employeeName}
//               onChange={(e) => {
//                 setemployeeName(e.target.value);
//               }}
//               class="form-control"
//               placeholder="Name"
//             />
//             <br />
//             <input
//               type="text"
//               value={employeeMobile}
//               onChange={(e) => {
//                 setemployeeMobile(e.target.value);
//               }}
//               class="form-control"
//               placeholder="Mobile No"
//             />
//             <br />
//             <input
//               type="Email"
//               value={employeeEmail}
//               onChange={(e) => {
//                 setemployeeEmail(e.target.value);
//               }}
//               class="form-control"
//               placeholder="Email"
//             />
//             <br />
//             <input
//               type="Password"
//               value={employeePassword}
//               onChange={(e) => {
//                 setemployeePassword(e.target.value);
//               }}
//               class="form-control"
//               placeholder="Password"
//             />
//             <br />
//             <input
//               type="date"
//               value={employeeDob}
//               onChange={(e) => {
//                 setemployeeDob(e.target.value);
//               }}
//               class="form-control"
//               placeholder="Date of Birth"
//             />
//             {/* <br />
//             <input
//               type="number"
//               value={theatre}
//               onChange={(e) => {
//                 setTheatre(e.target.value);
//               }}
//               class="form-control"
//               placeholder="Theatre Id"
//             />
//             <br /> */}
//             <button
//               className="btn btn-secondary btn-lg addbutton"
//               onClick={addEmployeeToDB}
//             >
//               Add
//             </button>
//           </td>
//         </tr>
//       </table>
//     </div>
//   );
// };
