import { BrowserRouter, Link, Route, Switch } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios";
import { url } from "../../../Common/common";
import "../css/List.css";
import { useHistory } from "react-router-dom";

const MovieUpdate = () => {
  const Movie = JSON.parse(localStorage.getItem("movie"));
  const movieId = Movie.movieId;
  const thumbnail = Movie.movieImage;
  const [movieName, setmovieName] = useState(Movie.movieName);
  const [movieImage, setmovieImage] = useState(Movie.movieImage);
  const [movieDescription, setmovieDescription] = useState(
    Movie.movieDescription
  );
  const [movieRuntime, setmovieRuntime] = useState(Movie.movieRuntime);
  const [movieTrailer, setMovieTrailer] = useState(Movie.movieTrailer);
  const [ageLimit, setageLimit] = useState(Movie.ageLimit);

  const history = useHistory();
  console.log(movieImage);

  const addMovieToDB = () => {
    if (movieName.length === 0) { 
      alert("Enter movie name");
    } else if (movieImage.length === 0) {
      alert("Enter Movie Image");
    } else if (movieDescription.length === 0) {
      alert("Enter movie description");
    } else if (movieRuntime.length === 0) {
      alert("Enter movie runtime");
    } else if (movieTrailer.length === 0) {
      alert("Enter trailer");
    } else if (ageLimit.length === 0) {
      alert("Enter movie age limit");
    } else {
      // when a file needs to be uploaded use FormData
      const data = new FormData();
      data.append("movieId", movieId);
      data.append("movieName", movieName);
      data.append("image", movieImage);
      data.append("movieDescription", movieDescription);
      data.append("movieTrailer", movieTrailer);
      data.append("movieRuntime", movieRuntime);
      data.append("ageLimit", ageLimit);

      axios.put(url + "/movie/update", data).then((response) => {
        // console.log("after req" + data);
        // console.log("put method call");
        const result = response.data;
        if (result == "success") {
          alert("successfully updated Movie");
          history.push("/movie");
        } else {
          alert("error while updating Movie");
        }
      });
    }
  };

  return (
    <div className="mainContainerDiv">
      <div>
        <img
          className="imageInMovieUpdate"
          src={url + "/image/" + thumbnail}
          alt="img"
        />
      </div>
      <div className="formDiv">
        <div className="inputDiv">
          <input
            type="text"
            value={movieName}
            onChange={(e) => {
              setmovieName(e.target.value);
            }}
            class="form-control"
            placeholder="Name"
          />
        </div>
        <div className="inputDiv">
          <input
            type="file"
            id="avatar"
            name="avatar"
            accept="image/png, image/jpeg"
            class="form-control"
            placeholder="Movie Image"
            onChange={(e) => {
              setmovieImage(e.target.files[0]);
            }}
          />
        </div>
        <div className="inputDiv">
          <input
            type="text"
            value={movieDescription}
            onChange={(e) => {
              setmovieDescription(e.target.value);
            }}
            class="form-control"
            placeholder="Movie Description"
          />
        </div>
        <div className="inputDiv">
          <input
            type="time"
            value={movieRuntime}
            onChange={(e) => {
              setmovieRuntime(e.target.value);
            }}
            class="form-control"
            placeholder="Movie Runtime"
          />
        </div>
        <div className="inputDiv">
          <input
            type="text"
            value={movieTrailer}
            onChange={(e) => {
              setMovieTrailer(e.target.value);
            }}
            class="form-control"
            placeholder="Movie Runtime"
          />
        </div>
        <div className="inputDiv">
          <input
            type="number"
            value={ageLimit}
            onChange={(e) => {
              setageLimit(e.target.value);
            }}
            class="form-control"
            placeholder="Movie Age limit"
          />
        </div>
        <div className="submitBtn" onClick={addMovieToDB}>
          Update
        </div>
      </div>
    </div>
  );
};

export default MovieUpdate;
