import { BrowserRouter, Link, Route, Switch } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios";
import { url } from "../../../Common/common";
import "../css/List.css";
import { useHistory } from "react-router-dom";
import trashImg from "../../../Images/trash.png";
import updateImg from "../../../Images/updated.png";

const Movie = () => {
  const history = useHistory();

  const [movieName, setmovieName] = useState("");
  const [movieImage, setmovieImage] = useState(undefined);
  const [movieDescription, setmovieDescription] = useState("");
  const [movieRuntime, setmovieRuntime] = useState("");
  const [ageLimit, setageLimit] = useState("");
  const [movieTrailer, setmovieTrailer] = useState("");
  const [changesMade, setChangesMade] = useState(false);

  const [movie, setmovie] = useState([]);

  // const [theatres, setTheatres] = useState();

  useEffect(() => {
    getaAllmovie();
  }, [changesMade]);

  // useEffect(() => {
  //   getaAllmovie();
  // }, [movieisdeleted]);

  const accessmovie = (e) => {
    const id = e.target.id;
    axios.get(url + "/movie/info/" + id).then((response) => {
      const result = response.data;

      if (result.status !== null) {
        alert("successfully access movie");

        localStorage.setItem("movie", JSON.stringify(result.status));
        history.push("/MovieUpdate");
      } else {
        alert("error while accessing  movie");
      }
    });
  };

  const deletemovie = (e) => {
    const id = e.target.id;
    axios.delete(url + "/movie/" + id).then((response) => {
      const result = response.data;
      if (result == "success") {
        {
          changesMade ? setChangesMade(false) : setChangesMade(true);
        }
        alert("successfully deleted movie");
      } else {
        alert("error while deleting movie");
      }
    });
  };

  const getaAllmovie = () => {
    axios.get(url + "/movie/getMovies").then((response) => {
      const result = response.data;
      setmovie(result.status);
    });
  };

  const addmovieToDB = () => {
    if (movieName.length === 0) {
      alert("Enter movie name");
    } else if (!movieImage) {
      alert("choose movie image");
    } else if (movieDescription.length === 0) {
      alert("Enter movie description");
    } else if (movieRuntime.length === 0) {
      alert("Enter movie runtime");
    } else if (ageLimit.length === 0) {
      alert("Enter movie age limit");
    } else {
      const data = new FormData();
      data.append("movieName", movieName);
      data.append("image", movieImage);
      data.append("movieDescription", movieDescription);
      data.append("movieRuntime", movieRuntime);
      data.append("ageLimit", ageLimit);
      data.append("movieTrailer", movieTrailer);

      axios.post(url + "/movie/addMovie", data).then((response) => {
        const result = response.data;
        if (result.status === "success") {
          {
            changesMade ? setChangesMade(false) : setChangesMade(true);
          }
          alert("successfully added an movie");
        } else {
          alert("error while adding movie");
        }
      });
    }
  };

  return (
    <div className="mainContainerDiv">
      <div className="formDiv">
        <div className="inputDiv">
          <input
            type="text"
            onChange={(e) => {
              setmovieName(e.target.value);
            }}
            class="form-control"
            placeholder="Name"
          />
        </div>
        <div className="inputDiv">
          <input
            type="file"
            id="avatar"
            name="avatar"
            accept="image/png, image/jpeg"
            class="form-control"
            placeholder="Movie Image"
            onChange={(e) => {
              setmovieImage(e.target.files[0]);
            }}
          />
        </div>
        <div className="inputDiv">
          <input
            type="text"
            onChange={(e) => {
              setmovieDescription(e.target.value);
            }}
            class="form-control"
            placeholder="Movie Description"
          />
        </div>
        <div className="inputDiv">
          <input
            type="text"
            onChange={(e) => {
              setmovieTrailer(e.target.value);
            }}
            class="form-control"
            placeholder="Movie Trailer"
          />
        </div>
        <div className="inputDiv">
          <input
            type="time"
            onChange={(e) => {
              setmovieRuntime(e.target.value);
            }}
            class="form-control"
            placeholder="Movie Runtime"
          />
        </div>
        <div className="inputDiv">
          <input
            type="number"
            onChange={(e) => {
              setageLimit(e.target.value);
            }}
            class="form-control"
            placeholder="Movie Age limit"
          />
        </div>
        <div onClick={addmovieToDB} className="submitBtn">
          Submit
        </div>
      </div>
      <div className="fetchedDataDiv">
        <div className="titleDiv">
          <div className="movieIddiv">Movie Id</div>
          <div className="movieNamediv">Movie name</div>
          <div className="deleteBtndiv">Delete</div>
          <div className="updateBtndiv">Update</div>
        </div>
        <div className="itemList">
          {movie.map((mov) => {
            return (
              <div className="itemRow">
                <div className="movieIddiv">{mov.movieId}</div>
                <div className="movieNamediv">{mov.movieName}</div>
                <div className="deleteBtndiv">
                  <img
                    className="deleteImg"
                    id={mov.movieId}
                    onClick={deletemovie}
                    src={trashImg}
                    alt="delete"
                  />
                </div>
                <div className="updateBtndiv">
                  <img
                    className="updateImg"
                    id={mov.movieId}
                    onClick={accessmovie}
                    src={updateImg}
                    alt="update"
                  />
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default Movie;

//             <tr>

//             </tr>
//           </thead>
//           <tbody>
//             {movie.map((mov) => {
//               return (
//                 <tr>
//                   <td> {mov.movieId} </td>
//                   <td>{mov.movieName} </td>
//                   <td>
//                     {" "}
//                     <button
//                       className="btn btn-secondary"
//                       id={mov.movieId}
//                       onClick={deletemovie}
//                     >
//                       delete
//                     </button>
//                   </td>
//                   <td>
//                     {" "}
//                     <button
//                       className="btn btn-secondary"
//                       id={mov.movieId}
//                       onClick={accessmovie}
//                     >
//                       Update
//                     </button>
//                   </td>
//                 </tr>
//               );
//             })}
//           </tbody>
//         </div>
//       </div>
//     </td>
//   </tr>
// </div>
// </div>
