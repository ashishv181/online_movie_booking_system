import { BrowserRouter, Link, Route, Switch } from "react-router-dom";
import "./Headerfooter.css";
import { useEffect, useState } from "react";
import axios from "axios";
import { url } from "../../../Common/common";

import { useHistory } from "react-router-dom";

const Update = () => {
const Manager = JSON.parse(localStorage.getItem("manager"));
//console.log(JSON.parse(Manager))

const [managerName, setManagerName] = useState(Manager.managerName);
const [managerMobile, setManagerMobile] = useState(Manager.managerMobile);
const [managerEmail, setManagerEmail] = useState(Manager.managerEmail);
const [managerPassword, setManagerPassword] = useState(
Manager.managerPassword
);
const [managerDob, setManagerDob] = useState(Manager.managerDob);
const [theatre, setTheatre] = useState(Manager.theatre_id);
const id = Manager.managerId;

const history = useHistory();

const addManagerToDB = () => {
if (managerName.length === 0) {
alert("Enter Name");
} else if (managerMobile.length === 0) {
alert("Enter MobileNo");
} else if (managerEmail.length === 0) {
alert("Enter Email");
} else if (managerPassword.length === 0) {
alert("Enter Password");
} else if (managerDob.length === 0) {
alert("Enter DOB");
} else if (theatre.length === 0) {
alert("Enter TheatreId");
} else {
// when a file needs to be uploaded use FormData
const data = new FormData();

      // add the data
      data.append("managerName", managerName);
      data.append("managerMobile", managerMobile);
      data.append("managerEmail", managerEmail);
      data.append("managerPassword", managerPassword);
      data.append("managerDob", managerDob);
      data.append("theatre", theatre);
      data.append("managerId", id);

      // send the data to the API
      axios.put(url + "/manager/" + id, data).then((response) => {
        console.log("put method call");
        const result = response.data;
        if (result.status === "success") {
          alert("successfully added an manager");
          history.push("/manager");
        } else {
          alert("error while adding manager");
        }
      });
    }

};

return (
<div className="centrealign container">
<table className="tablepadding">
<tr>
<td>
<input
type="text"
value={managerName}
onChange={(e) => {
setManagerName(e.target.value);
}}
class="form-control"
placeholder="Name"
/>
<br />
<input
type="text"
value={managerMobile}
onChange={(e) => {
setManagerMobile(e.target.value);
}}
class="form-control"
placeholder="Mobile No"
/>
<br />
<input
type="Email"
value={managerEmail}
onChange={(e) => {
setManagerEmail(e.target.value);
}}
class="form-control"
placeholder="Email"
/>
<br />
<input
type="Password"
value={managerPassword}
onChange={(e) => {
setManagerPassword(e.target.value);
}}
class="form-control"
placeholder="Password"
/>
<br />
<input
type="date"
value={managerDob}
onChange={(e) => {
setManagerDob(e.target.value);
}}
class="form-control"
placeholder="Date of Birth"
/>
<br />
<input
type="number"
value={theatre}
onChange={(e) => {
setTheatre(e.target.value);
}}
class="form-control"
placeholder="Theatre Id"
/>
<br />
<button
              className="btn btn-secondary btn-lg addbutton"
              onClick={addManagerToDB}
            >
Add
</button>
</td>
</tr>
</table>
</div>
);
};

export default Update;
