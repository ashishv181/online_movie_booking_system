import { BrowserRouter, Link, Route, Switch } from "react-router-dom";
import "./Headerfooter.css";
import { useEffect, useState } from "react";
import axios from "axios";
import { url } from "../../../Common/common";
import EmployeeUpdate from "./EmployeeUpdate";
import { useHistory } from "react-router-dom";
import "../css/List.css";
import trashImg from "../../../Images/trash.png";
import updateImg from "../../../Images/updated.png";

const Employee = () => {
  const history = useHistory();

  const [employeeName, setemployeeName] = useState("");
  const [employeeMoblie, setemployeeMoblie] = useState("");
  const [employeeEmail, setemployeeEmail] = useState("");
  const [employeePassword, setemployeePassword] = useState("");
  const [employeeDob, setemployeeDob] = useState("");
  const [employee, setemployee] = useState([]);
  const [changesMade, setChangesMade] = useState(false);
  const theatreId = JSON.parse(localStorage.getItem("myUser")).theatre
    .theatreId;

  useEffect(() => {
    getaAllEmployee();
  }, [changesMade]);

  // useEffect(() => {
  //   getaAllEmployee();
  // }, [Employeeisdeleted]);

  const accessEmployee = (e) => {
    const id = e.target.id;
    axios.get(url + "/employee/" + id).then((response) => {
      const result = response.data;

      if (result != null) {
        alert("successfully access Employee");
        localStorage.setItem("employee", JSON.stringify(result));
        history.push("/EmployeeUpdate");
      } else {
        alert("error while accessing  Employee");
      }
    });
  };

  const deleteEmployee = (e) => {
    const id = e.target.id;
    axios.delete(url + "/employee/" + id).then((response) => {
      const result = response.data;
      if (result == "success") {
        {
          changesMade ? setChangesMade(false) : setChangesMade(true);
        }
        alert("successfully deleted an Employee");
      } else {
        alert("error while deleting  Employee");
      }
    });
  };
  const getaAllEmployee = () => {
    axios.get(url + "/employee/getEmployees").then((response) => {
      const result = response.data;
      setemployee(result);
    });
  };

  const addEmployeeToDB = () => {
    if (employeeName.length === 0) {
      alert("Enter Name");
    } else if (employeeMoblie.length === 0) {
      alert("Enter MobileNo");
    } else if (employeeEmail.length === 0) {
      alert("Enter Email");
    } else if (employeePassword.length === 0) {
      alert("Enter Password");
    } else if (employeeDob.length === 0) {
      alert("Enter DOB");
    } else {
      // when a file needs to be uploaded use FormData
      const data = new FormData();

      // add the data
      data.append("employeeName", employeeName);
      data.append("employeeMoblie", employeeMoblie);
      data.append("employeeEmail", employeeEmail);
      data.append("employeePassword", employeePassword);
      data.append("employeeDob", employeeDob);
      data.append("theatre", theatreId);

      // send the data to the API
      axios.post(url + "/employee/addEmployee", data).then((response) => {
        const result = response.data;
        if (result != null) {
          {
            changesMade ? setChangesMade(false) : setChangesMade(true);
          }
          alert("successfully added an Employee");
        } else {
          alert("error while adding Employee");
        }
      });
    }
  };

  return (
    <div className="mainContainerDiv">
      <div className="formDiv">
        <div className="inputDiv">
          <input
            type="text"
            onChange={(e) => {
              setemployeeName(e.target.value);
            }}
            class="form-control"
            placeholder="Name"
          />
        </div>
        <div className="inputDiv">
          <input
            type="text"
            onChange={(e) => {
              setemployeeMoblie(e.target.value);
            }}
            class="form-control"
            placeholder="Mobile No"
          />
        </div>
        <div className="inputDiv">
          <input
            type="Email"
            onChange={(e) => {
              setemployeeEmail(e.target.value);
            }}
            class="form-control"
            placeholder="Email"
          />
        </div>
        <div className="inputDiv">
          <input
            type="Password"
            onChange={(e) => {
              setemployeePassword(e.target.value);
            }}
            class="form-control"
            placeholder="Password"
          />
        </div>
        <div className="inputDiv">
          <input
            type="date"
            onChange={(e) => {
              setemployeeDob(e.target.value);
            }}
            class="form-control"
            placeholder="Date of Birth"
          />
        </div>
        <div className="submitBtn" onClick={addEmployeeToDB}>
          Add
        </div>
      </div>
      <div className="fetchedDataDiv">
        <div className="titleDiv">
          <div className="movieIddiv">Employee Id</div>
          <div className="movieNamediv">Employee name</div>
          <div className="deleteBtndiv">Delete</div>
          <div className="updateBtndiv">Update</div>
        </div>
        <div className="itemList">
          {employee.map((emp) => {
            return (
              <div className="itemRow">
                <div className="movieIddiv">{emp.employeeId}</div>
                <div className="movieNamediv">{emp.employeeName}</div>
                <div className="deleteBtndiv">
                  <img
                    className="deleteImg"
                    id={emp.employeeId}
                    onClick={deleteEmployee}
                    src={trashImg}
                    alt="delete"
                  />
                </div>
                <div className="updateBtndiv">
                  <img
                    className="updateImg"
                    id={emp.employeeId}
                    onClick={accessEmployee}
                    src={updateImg}
                    alt="update"
                  />
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};
export default Employee;

// {
//   <div className="container table1">
//   <table>
//     <tr>
//       <td>
//         <div class="form-floating mb-6">
//           <table className="tablepadding">
//             <tr>
//               <td>
//                 <input
//                   type="text"
//                   onChange={(e) => {
//                     setemployeeName(e.target.value);
//                   }}
//                   class="form-control"
//                   placeholder="Name"
//                 />
//                 <br />
//                 <input
//                   type="text"
//                   onChange={(e) => {
//                     setemployeeMoblie(e.target.value);
//                   }}
//                   class="form-control"
//                   placeholder="Mobile No"
//                 />
//                 <br />
//                 <input
//                   type="Email"
//                   onChange={(e) => {
//                     setemployeeEmail(e.target.value);
//                   }}
//                   class="form-control"
//                   placeholder="Email"
//                 />
//                 <br />
//                 <input
//                   type="Password"
//                   onChange={(e) => {
//                     setemployeePassword(e.target.value);
//                   }}
//                   class="form-control"
//                   placeholder="Password"
//                 />
//                 <br />
//                 <input
//                   type="date"
//                   onChange={(e) => {
//                     setemployeeDob(e.target.value);
//                   }}
//                   class="form-control"
//                   placeholder="Date of Birth"
//                 />
//                 <br />
//                 <input
//                   type="number"
//                   onChange={(e) => {
//                     setTheatre(e.target.value);
//                   }}
//                   class="form-control"
//                   placeholder="Theatre Id"
//                 />
//                 <br />
//                 <button
//                   className="btn btn-secondary btn-lg addbutton"
//                   onClick={addEmployeeToDB}
//                 >
//                   Add
//                 </button>
//               </td>
//             </tr>
//           </table>
//         </div>
//       </td>
//       <td>
//         <div className="table2 ">
//           <table>
//             <thead>
//               <tr>
//                 <th className="tablecolumn">Employee Id</th>
//                 <th className="tablecolumn">Employee Name</th>
//                 <th className="tablecolumn">Delete</th>
//                 <th className="tablecolumn">Update</th>
//               </tr>
//             </thead>
//             <tbody>
//               {employee.map((emp) => {
//                 return (
//                   <tr>
//                     <td> {emp.employeeId} </td>
//                     <td>{emp.employeeName} </td>
//                     <td>
//                       {" "}
//                       <button
//                         className="btn btn-secondary"
//                         id={emp.employeeId}
//                         onClick={deleteEmployee}
//                       >
//                         delete
//                       </button>
//                     </td>
//                     <td>
//                       {" "}
//                       <button
//                         className="btn btn-secondary"
//                         id={emp.employeeId}
//                         onClick={accessEmployee}
//                       >
//                         Update
//                       </button>
//                     </td>
//                   </tr>
//                 );
//               })}
//             </tbody>
//           </table>
//         </div>
//       </td>
//     </tr>
//   </table>
// </div>
// );
// };
