import { useLocation, useHistory } from "react-router";
import { url } from "../../../Common/common";
import axios from "axios";
import { useState, useEffect } from "react";
// import "./css/ShowsByTheatre.css";

const OfflineBooking = () => {
  const theatreId = JSON.parse(localStorage.getItem("myUser")).theatre
    .theatreId;
  console.log("th id: " + theatreId);
  const history = useHistory();
  const [shows, setShows] = useState([]);

  useEffect(() => {
    getShowsByTheatreId();
  }, []);

  const getShowsByTheatreId = () => {
    axios.get(url + "/show/getShowByTheatre/" + theatreId).then((response) => {
      const result = response.data;
      if (result != null) {
        // alert("Success getting shows!");
        setShows(result);
        //history.push('/theatre-info', result.status)
      } else {
        alert("error occured while getting shows!");
      }
    });
  };
  const theatreRedirect = (shw) => {
    // console.log("showId: " + shw.showId);
    // console.log("the id: " + shw.theatre.theatreId);
    history.push("/offline-theatre-info", {
      showId: shw.showId,
      movieId: shw.movie.movieId,
      theatreId: shw.theatre.theatreId,
    });
  };
  const movieDetailsRedirect = (id) => {
    console.log("id" + id);
    axios.get(url + "/movie/info/" + id).then((response) => {
      const result = response.data;
      if (result.status != null) {
        history.push("/theatreToMovieInfo", { moviexxx: result.status });
      } else {
        alert("error occured while getting movie detail");
      }
    });
  };

  return (
    <div className="showListContained container">
      <table
        className="table table-borderless
        "
      >
        <thead>
          <th className="tableColumn"></th>

          <th className="tableColumn">Movie Name</th>
          <th className="tableColumn">Theatre Name</th>
          <th className="tableColumn">Show Start Time</th>
          <th className="tableColumn">Show End Time</th>
        </thead>
        <tbody>
          {shows.map((shw) => {
            return (
              <tr>
                <td
                  onClick={() => {
                    movieDetailsRedirect(shw.movie.movieId);
                  }}
                >
                  <img
                    className="imagCSS imageHover"
                    src={url + "/image/" + shw.movie.movieImage}
                  />
                </td>

                <td>{shw.movie.movieName}</td>
                <td>{shw.theatre.theatreName}</td>
                <td>{shw.showStartTime}</td>
                <td>{shw.showEndTime}</td>

                <td>
                  <button
                    onClick={() => {
                      theatreRedirect(shw);
                    }}
                    id={shw.theatre.theatreId}
                  >
                    Book
                  </button>{" "}
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

export default OfflineBooking;
