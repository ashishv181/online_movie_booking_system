import { useLocation } from "react-router";
import "./OfflineTicketBooked.css";

const OfflineTicketBooked = () => {
  const location = useLocation();
  const invoice = location.state.showDetail;
  const seats = location.state.bookedSeat;
  const amountPaid = location.state.amount;
  let signedInUser = JSON.parse(localStorage.getItem("myUser"));
  console.log(invoice);

  // userName
  // movieName
  // theatreName
  // show Start Time
  // Theatre Address
  // seats booked
  // payment amount

  return (
    <div className="container invoiceContainer">
      <table className="table table-borderless">
        {/* <tr>
          <td className="boldCall">User Name</td>
          <td className="centerColumn">:</td>
          <td>{signedInUser.userName}</td>
        </tr> */}
        <tr>
          <td className="boldCall">Movie</td>
          <td className="centerColumn">:</td>
          <td>{invoice.movie.movieName}</td>
        </tr>
        <tr>
          <td className="boldCall">Theatre</td>
          <td className="centerColumn">:</td>
          <td>{invoice.theatre.theatreName}</td>
        </tr>
        <tr>
          <td className="boldCall">Show Start Time</td>
          <td className="centerColumn">:</td>
          <td>{invoice.showStartTime}</td>
        </tr>
        <tr>
          <td className="boldCall">Address</td>
          <td className="centerColumn">:</td>
          <td>{invoice.theatre.theatreAddress}</td>
        </tr>
        <tr>
          <td className="boldCall">Seats Booked</td>
          <td className="centerColumn">:</td>
          <td>{seats}</td>
        </tr>
        <tr>
          <td className="boldCall">Amount Paid</td>
          <td className="centerColumn">:</td>
          <td>{amountPaid}</td>
        </tr>
      </table>
    </div>
  );
};

export default OfflineTicketBooked;
