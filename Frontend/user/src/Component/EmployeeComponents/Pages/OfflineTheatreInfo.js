import { useLocation, useHistory } from "react-router";
import { useState } from "react";
import { useEffect } from "react";
import axios from "axios";
import { url } from "../../../Common/common";
import "./OfflineTheatreInfo.css";

const OfflineTheatreInfo = () => {
  const history = useHistory();
  const location = useLocation();
  const showId = location.state.showId;
  const theatreId = location.state.theatreId;
  const movieId = location.state.movieId;
  let seatsSelected = 0;
  let bookingAmount = 0;

  const [showDetails, setShowDetails] = useState([]);
  const [seatsRemaining, setSeatsRemaining] = useState(
    showDetails.noOfSeatsLeft
  );
  const [userMobile, ssetUsermobile] = useState();
  const [bookingAmounts, setBookingAmount] = useState(0);
  //let seatsLeft = seatsRemaining;
  useEffect(() => {
    console.log("useEffect called");
    getShowsDetailsforSelectedTheatre();
    //setSeatsRemaining(seatsRemaining - seatsSelected);
    //seatsLeft = seatsRemaining
    //bookClicked();
    setSeatsRemaining(showDetails.noOfSeatsLeft);
  }, [showDetails.noOfSeatsLeft]);

  const onSeatsBooked = (event) => {
    seatsSelected = event.target.value;

    console.log("seats selected: " + seatsSelected);
  };

  // const bookClicked = () => {
  //   setSeatsRemaining(seatsRemaining - seatsSelected);
  //   setBookingAmount(seatsSelected * 150)
  // };
  console.log("seats left: " + seatsRemaining);

  //console.log(userLogging)

  const getShowsDetailsforSelectedTheatre = () => {
    axios.get(url + "/show/" + showId).then((response) => {
      const result = response.data;
      if (result.status != null) {
        setShowDetails(result.status);

        // history.push("/ticket-booking", time);
      } else {
        alert("error occured while getting time");
      }
    });
  };

  const ticketBooked = () => {
    setSeatsRemaining(seatsRemaining - seatsSelected);
    bookingAmount = seatsSelected * 150;
    setBookingAmount(bookingAmount);
    const data = new FormData();
    data.append("show", showId);
    data.append("seatsBooked", seatsSelected);
    data.append("bookingAmount", bookingAmount);
    data.append("userMobile", userMobile);

    axios.post(url + "/offlineBook/book", data).then((response) => {
      const result = response.data;
      if (result.status === "success") {
        alert("Ticked Booked");
        const data2 = new FormData();
        data2.append("noOfSeatsLeft", seatsRemaining - seatsSelected);
        axios.put(url + "/show/bookShow/" + showId, data2).then((response) => {
          const result = response.data;
          if (result.status != null) {
            alert("Ticketed");

            history.push("/offline-ticket-booked", {
              showDetail: showDetails,
              bookedSeat: seatsSelected,
              amount: bookingAmount,
            });
          } else {
            alert("error occured in database");
          }
        });
        //history.push("/ticket-booked", {data : data});
      } else {
        alert("error occured while booking ticket");
      }
    });
  };

  console.log(showDetails);

  return (
    <div className="">
      {showDetails.length != 0 && (
        <div className="theatreListContainer container">
          <table className="table table-borderless ">
            <thead>
              <tr>
                <th className="tableTheatreColumn"></th>
                <th className="tableTheatreColumn">Theatre Name</th>
                <th className="tableTheatreColumn">Address</th>
                <th className="tableTheatreColumn">City</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <img
                    className="imageCss"
                    src={url + "/image/" + showDetails.movie.movieImage}
                  />
                </td>
                <td className="detailsCss">
                  <b>{showDetails.theatre.theatreName}</b>
                </td>
                <td className="detailsCss">
                  <b>{showDetails.theatre.theatreAddress}</b>
                </td>
                <td className="detailsCss">
                  <b>{showDetails.theatre.theatreCity}</b>
                </td>
              </tr>
              <tr>
                <td></td>
                <td></td>

                <td colSpan="2">
                  <label Style="display:inline-block; margin-right: 5px; font-weight:bold;">
                    User Mobile
                  </label>
                  <input
                    type="text"
                    onChange={(e) => {
                      ssetUsermobile(e.target.value);
                    }}
                  />
                </td>
              </tr>
              <tr>
                <td colSpan="2">
                  <span className="seatLabelFont">Enter Seats</span>
                  <input
                    name="counter"
                    onChange={onSeatsBooked}
                    min="1"
                    max={seatsRemaining}
                    type="number"
                  />
                  <button onClick={ticketBooked}>Book</button>
                </td>
                <td colSpan="2">
                  <span className="detailsCss">
                    <b>Booking Amount</b>
                  </span>
                  <span>
                    <input type="text" value={bookingAmounts} />
                  </span>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      )}
    </div>
  );
};

export default OfflineTheatreInfo;

// import { useLocation, useHistory } from "react-router"
// import { useState } from "react"
// import { useEffect } from "react"
// import axios from "axios"
// import { url } from "../Common/common"
// import '../Component/TheatreInfo.css'

// const TheatreInfo = () => {
//   const history = useHistory();
//   const location = useLocation();
//   const showId = location.state.showId;
//   const theatreId = location.state.theatreId;
//   const movieId = location.state.movieId;
//   let seatsSelected = 0;

//   const [showDetails, setShowDetails] = useState([]);
//   const [seatsRemaining, setSeatsRemaining] = useState(
//     showDetails.noOfSeatsLeft
//   );

//   useEffect(() => {
//     console.log("useEffect called");
//     getShowsDetailsforSelectedTheatre();
//     setSeatsRemaining(showDetails.noOfSeatsLeft);
//   }, [showDetails.noOfSeatsLeft]);

//   const onSeatsBooked = (event) => {
//     seatsSelected = event.target.value;
//     console.log("seats selected: " + seatsSelected);
//   };

//   const bookClicked = (event) => {
//     setSeatsRemaining(seatsRemaining - seatsSelected);
//     console.log("seats left: " + seatsRemaining);
//   };

//   const getShowsDetailsforSelectedTheatre = () => {
//     axios.get(url + "/show/" + showId).then((response) => {
//       const result = response.data;
//       if (result.status != null) {
//         setShowDetails(result.status);

//         // history.push("/ticket-booking", time);
//       } else {
//         alert("error occured while getting time");
//       }
//     });
//   };

//   console.log(showDetails);

//   return (
//     <div className="">
//       {showDetails.length != 0 && (
//         <div>
//           <h1></h1>
//           <span><img className="imageCss" src={url + "/image/" + showDetails.movie.movieImage} /></span>
//           <span className="detailsCss"><b>{showDetails.theatre.theatreName}</b></span>
//           <span className="detailsCss"><b>{showDetails.theatre.theatreAddress}</b></span>
//           <span className="detailsCss"><b>{showDetails.theatre.theatreCity}</b></span>
//           <label>Enter Seats</label>
//           <input
//             onChange={onSeatsBooked}
//             min="1"
//             max={seatsRemaining}
//             type="number"
//           />
//           <button onClick={bookClicked}>Book</button>
//           <span>helllooo</span>
//         </div>
//       )}
//     </div>
//   );
// };

// export default TheatreInfo
