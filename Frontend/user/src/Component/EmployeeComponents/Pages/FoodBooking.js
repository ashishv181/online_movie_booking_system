import axios from "axios";
import { url } from "../../../Common/common";
import React, { useState, useEffect } from "react";
import "./FoodBooking.css";

const FoodBook = () => {
  const [foodItems, setFoodItems] = useState([]);
  const [counterValue, setCounterValue] = useState(0);
  const [currentId, setCurrentId] = useState(0);

  useEffect(() => {
    getFoodItems();
  }, []);

  const getFoodItems = () => {
    axios.get(url + "/employee/foodItems").then((response) => {
      const result = response.data;
      if (result.status == null) {
        alert("error fetching food items");
      } else {
        setFoodItems(result.status);
      }
    });
  };

  const calculateValue = (id, price) => {
    if (id == currentId) {
      let total = price * counterValue;
      return total;
    }
  };

  return (
    <div className="foodContainer">
      <div className="foodItemHeader">
        <div className="itemImage"></div>
        <div className="itemName">Item Name</div>
        <div className="itemPrice">Price</div>
        <div className="itemQuantity">Quantity</div>
        <div className="itemTotal">Amount</div>
      </div>
      {foodItems.map((attr) => {
        return (
          <div key={attr.foodId} className="foodItemContainer">
            <div className="foodImageDiv">
              <img
                className="foodImage"
                src={url + "/image/" + attr.foodItemImage}
              />
            </div>
            <div className="foodItems">{attr.foodItemName}</div>
            <div className="foodPrice">{attr.foodItemPrice}</div>
            <div className="counterContainer">
              <input
                type="number"
                defaultValue="0"
                min="0"
                max="5"
                onChange={(e) => {
                  setCounterValue(e.target.value);
                  setCurrentId(attr.foodId);
                }}
              />
            </div>
            <input
              defaultValue="0"
              className="itemTotal"
              type="number"
              value={calculateValue(attr.foodId, attr.foodItemPrice)}
            />
          </div>
        );
      })}
    </div>
  );
};
export default FoodBook;
