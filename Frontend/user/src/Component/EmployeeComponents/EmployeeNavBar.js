// import "./Employee.css";
import "../UserComponent/css/NavBar.css";
import { BrowserRouter, Link, Route, Switch, NavLink } from "react-router-dom";
import EditProfile from "../../CommonPages/SignIn-Up-Edit/EditProfile";
import Food from "./Pages/FoodBooking";
import { useContext, useState } from "react";
import AuthContext from "../../Common/auth-context";
import OfflineBooking from "./Pages/OfflineBooking";
import OfflineTheatreInfo from "./Pages/OfflineTheatreInfo";
import OfflineTicketBooked from "./Pages/OffileTicketBooked";

const EmpNavBar = () => {
  const { user, setUser } = useContext(AuthContext);
  const [firstName, setName] = useState(user.employeeName.split(" ")[0]);

  const logOutHandler = () => {
    localStorage.removeItem("myUser");
    localStorage.removeItem("role");
    localStorage.removeItem("employee");
    localStorage.removeItem("show");
    localStorage.removeItem("movie");
    setUser(null);
  };

  const somPart = () => {
    return (
      <ul
        className="dropdown-menu dropDownContent dropdown-menu-end "
        aria-labelledby="navbarDropdownMenuLink"
      >
        <li>
          <NavLink
            className="nav-link dropdown-item myDropdown"
            to="/editprofile"
          >
            <b>Edit Profile</b>
          </NavLink>
        </li>
        <li>
          <div
            onClick={logOutHandler}
            className="nav-link dropdown-item myDropdown"
            to="/signout"
          >
            <b>Sign Out</b>
          </div>
        </li>
      </ul>
    );
  };

  return (
    <div>
      <BrowserRouter>
        <nav className="navbar navbar-expand-lg navbar-dark navContainer">
          <div className="container-fluid">
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarNav"
              aria-controls="navbarNav"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
              <ul className="navbar-nav">
                <li className="nav-item">
                  <Link className="nav-link" to="/offlineBooking">
                    <button className="btn btn-dark">Offline Booking</button>
                  </Link>
                </li>
                <li>
                  <Link className="nav-link" to="/foodAndBeverage">
                    <button className="btn btn-dark">Food Booking</button>
                  </Link>
                </li>

                {/* <li className="nav-item">
                  <Link className="nav-link" to="/foodAndBeverage">
                    <button className="btn btn-secondary">
                      Food And Beverage
                    </button>
                  </Link>
                </li> */}

                <div className="flex">
                  <li className="nav-item dropDownContainer dropdown">
                    <a
                      className="nav-link {/*dropdown-toggle*/} "
                      href="#"
                      id="navbarDropdownMenuLink"
                      role="button"
                      data-bs-toggle="dropdown"
                      aria-expanded="false"
                    >
                      {user != null && (
                        <div className="userName">Hi {firstName}!</div>
                      )}
                    </a>
                    <ul
                      className="dropdown-menu dropDownContent dropdown-menu-end "
                      aria-labelledby="navbarDropdownMenuLink"
                    >
                      <li>
                        <NavLink
                          className="nav-link dropdown-item myDropdown"
                          to="/editprofile"
                        >
                          <b>Edit Profile</b>
                        </NavLink>
                      </li>
                      <li>
                        <div
                          onClick={logOutHandler}
                          className="nav-link dropdown-item myDropdown"
                        >
                          <b>Sign Out</b>
                        </div>
                      </li>
                    </ul>
                  </li>
                </div>
              </ul>
            </div>
          </div>
        </nav>

        <Switch>
          <Route path="/editprofile" component={EditProfile} />
          <Route path="/foodAndBeverage" component={Food} />
          <Route path="/offlineBooking" component={OfflineBooking} />
          <Route path="/offline-theatre-info" component={OfflineTheatreInfo} />
          <Route
            path="/offline-ticket-booked"
            component={OfflineTicketBooked}
          />
        </Switch>
      </BrowserRouter>
    </div>
  );
};

export default EmpNavBar;
