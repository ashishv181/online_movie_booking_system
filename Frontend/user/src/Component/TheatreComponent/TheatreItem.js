import styled from "styled-components";

export default styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 450px;
  width: 100%;
  background-color: inherit;
  padding: 10px;
  color: #fff;
  margin: 10px 2px 0px 0px;
  font-size: 4em;
  cursor: pointer;
`;
