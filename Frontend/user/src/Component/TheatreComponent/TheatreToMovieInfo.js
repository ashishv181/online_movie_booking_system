import { useLocation, useHistory } from "react-router";
import { useState } from "react";
import { useEffect } from "react";
import axios from "axios";
import { url } from "../../Common/common";
import { YoutubePlayer } from "reactjs-media";
// import '../Component/VideoPlayer.css'

const TheatreToMovieInfo = () => {
  const location = useLocation();
  const movie = location.state.moviexxx;

  console.log(movie);

  const [shows, setShows] = useState([]);
  useEffect(() => {
    // showDetailsforMovie();
  }, [shows]);

  const history = useHistory();

  // const showDetailsforMovie = () => {
  //   axios.get(url + "/show/getShow/" + movie.movieId).then((response) => {
  //     const result = response.data;
  //     if (result != null) {
  //       setShows([...result]);
  //     } else {
  //       alert("error occured while getting shows");
  //     }
  //   });
  // };

  const theatreRedirect = (shw) => {
    // console.log("showId: " + shw.showId);
    // console.log("the id: " + shw.theatre.theatreId);
    history.push("/theatre-info", {
      showId: shw.showId,
      movieId: shw.movie.movieId,
      theatreId: shw.theatre.theatreId,
    });
  };

  return (
    <div className="MainVideo">
      <div>
        <YoutubePlayer
          src={movie.movieTrailer} // Reqiured
          width={1000}
          height={500}
        />
      </div>
      {/* <div className="info"><b>{movie.movieTrailer}</b></div> */}
      <div className="info">
        <b>{movie.movieDescription}</b>
      </div>
      <div className="ageLimit">
        <h2>
          <b>{movie.ageLimit}</b>
        </h2>
      </div>

      {/* <div >
        <button onClick={showDetailsforMovie}>Get Shows</button>
      </div> */}
      {/* <div className="showListContainer">
        {shows.map((shw) => {
          return (
            <table>
              <tbody>
                <td>{shw.showId}</td>
                <td>{shw.movie.movieName}</td>
                <td>{shw.theatre.theatreName}</td>
                <button
                  onClick={() => {
                    theatreRedirect(shw);
                  }}
                  id={shw.theatre.theatreId}
                >
                  Book
                </button>
              </tbody>
            </table>
          );
        })}
      </div> */}
    </div>
  );
};

export default TheatreToMovieInfo;
