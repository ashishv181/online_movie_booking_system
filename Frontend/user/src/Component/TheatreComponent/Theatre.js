import Item from "./TheatreItem";
import { url } from "../../Common/common";
import { useEffect, useState } from "react";
import { useHistory } from "react-router";
import axios from "axios";
import { useLocation } from "react-router";
import "./css/Theatre.css";

const Theatre = ({word}) => {
  const [theatres, setTheatres] = useState([]);

  const history = useHistory();

  useEffect(() => {
    getTheatre();
  }, []);

  const getTheatre = () => {
    axios.get(url + "/theatre/").then((response) => {
      const result = response.data;
      if (result != null) {
        // alert("Got theatres!");
        setTheatres(result);
      } else {
        alert("error occured while getting theatres");
      }
    });
  };

  const getInfoOfSelectedTheatre = (id) => {
    history.push("/showBytheatre", { id: id });
  };
  return (
    <>
      <div class="grid">
        <div className="row allTheatreContainer">
          {theatres.filter(t => t.theatreName.toUpperCase().includes(word.toUpperCase())).map((theatre) => {
            return (
              <div
                className="col-md-3 singleTheatreDiv"
                onClick={() => {
                  getInfoOfSelectedTheatre(theatre.theatreId);
                }}
              >
                <div>
                  <div>{theatre.theatreName}</div>
                  <div>{theatre.theatreAddress}</div>
                  <div>{theatre.theatreCity}</div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </>
  );
};

export default Theatre;

//   import img1 from '../Images/image01.jpg'
// import img2 from '../Images/image02.jpg'
// import img3 from '../Images/imag03.jpg'
// import img4 from '../Images/imag04.jpg'
// import img5 from '../Images/image06.jpg'
// < div class="row movie-app border" >
//         <div class="col-sm-3"><img className="style" src={img1} /></div>
//         <div class="col-sm-3"><img className="style" src={img2}/></div>
//         <div class="col-sm-3"><img className="style" src={img3}/></div>
//         <div class="col-sm-3"><img className="style" src={img4}/></div>
//         <div class="col-sm-3"><img className="style" src={img5}/></div>
//   </div >
