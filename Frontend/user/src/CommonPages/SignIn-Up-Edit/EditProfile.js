import classes from "./css/EditProfile.module.css";
import { useState } from "react";
import Input from "./Input";
import axios from "axios";
import { url } from "../../Common/common";

const EditProfile = () => {
  const loggedInUser = JSON.parse(localStorage.getItem("myUser"));
  const role = JSON.parse(localStorage.getItem("role"));
  let name = "";
  let password = "";
  let email = "";
  let birth = "";
  let mobile = "";

  if (role == "manager") {
    name = loggedInUser.managerName;
    password = loggedInUser.managerPassword;
    email = loggedInUser.managerEmail;
    birth = loggedInUser.managerDob;
    mobile = loggedInUser.managerMoblie;
  } else if (role == "admin") {
    name = loggedInUser.adminName;
    password = loggedInUser.adminPassword;
    email = loggedInUser.adminEmail;
    birth = loggedInUser.adminDob;
    mobile = loggedInUser.adminMobile;
  } else if (role == "employee") {
    name = loggedInUser.employeeName;
    password = loggedInUser.employeePassword;
    email = loggedInUser.employeeEmail;
    birth = loggedInUser.employeeDob;
    mobile = loggedInUser.employeeMobile;
  } else {
    name = loggedInUser.userName;
    password = loggedInUser.userPassword;
    email = loggedInUser.userEmail;
    birth = loggedInUser.userdob;
    mobile = loggedInUser.userPhone;
  }

  console.log(name);
  console.log(email);
  console.log(password);
  console.log(birth);
  console.log(mobile);

  const [somethingToChange, change] = useState(false);
  let enteredName = "";
  let enteredEmail = "";
  let enteredPassword = "";
  let enteredMobile = "";
  let enteredBirth = "";

  const updateEnteredName = (value) => {
    enteredName = value;
  };
  const updateEnteredEmail = (value) => {
    enteredEmail = value;
  };
  const updateEnteredPassword = (value) => {
    enteredPassword = value;
  };
  const updateMobile = (value) => {
    enteredMobile = value;
  };
  const updateBirth = (value) => {
    enteredBirth = value;
  };

  const submitHandler = (event) => {
    event.preventDefault();
    const data = new FormData();
    data.append("userName", enteredName);
    data.append("userEmail", enteredEmail);
    data.append("userPassword", enteredPassword);
    data.append("userPhone", enteredMobile);
    data.append("userdob", enteredBirth);
    data.append("userId", loggedInUser.userId);
    if (somethingToChange === true) {
      change(false);
    } else {
      change(true);
    }

    axios.put(url + "/user/edit", data).then((response) => {
      const result = response.data;
      if (result.status === null) {
        alert("log in error");
      } else {
        // localStorage.clear();
        const editedUser = JSON.stringify(result.status);
        localStorage.setItem("myUser", editedUser);
        alert("success");
      }
    });
  };

  const Inputs = [
    {
      key: 1,
      id: 1,
      label: "Name",
      type: "text",
      afterUpdate: updateEnteredName,
      setValue: name,
    },
    {
      key: 2,
      id: 2,
      label: "Email",
      type: "text",
      afterUpdate: updateEnteredEmail,
      setValue: email,
    },
    {
      key: 3,
      id: 3,
      label: "Password",
      type: "password",
      afterUpdate: updateEnteredPassword,
      setValue: password,
    },
    {
      key: 4,
      id: 4,
      label: "D.O.B",
      type: "date",
      afterUpdate: updateBirth,
      setValue: birth,
    },
    {
      key: 5,
      id: 5,
      label: "Mobile",
      type: "text",
      afterUpdate: updateMobile,
      setValue: mobile,
    },
  ];

  return (
    <div className={classes.formContainer}>
      <form onSubmit={submitHandler}>
        {Inputs.map((attr) => {
          return (
            <Input
              id={attr.id}
              name={attr.label}
              type={attr.type}
              afterUpdate={attr.afterUpdate}
              changer={somethingToChange}
              values={attr.setValue}
            />
          );
        })}
        <button
          className={classes.submitBtn}
          type="submit"
          // onClick={handleReset}
        >
          Submit
        </button>
      </form>
    </div>
  );
};
export default EditProfile;
