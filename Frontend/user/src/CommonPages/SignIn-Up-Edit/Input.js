import { useState, useEffect } from "react";
import classes from "./css/Input.module.css";

const Input = (props) => {
  const [enteredInput, setEnteredInput] = useState(props.values);
  const [inputTouched, setInputTouched] = useState(false);
  
  // console.log(enteredInput);
  // useEffect(() => {
  //   setEnteredInput("");
  //   setInputTouched(false);
  // }, [props.changer]);

  const enteredInputIsNotBlank = enteredInput.length !== 0;

  let enteredInputIsInValid = !enteredInputIsNotBlank && inputTouched;

  const inputChangeHandler = (event) => {
    setEnteredInput(event.target.value);
  };

  const inputBlurHandler = (event) => {
    setInputTouched(true);
  };

  props.afterUpdate(enteredInput);

  return (
    <div className={classes.formActions}>
      <div className={classes.label}>
        <label htmlFor={props.id}>{props.name} </label>
      </div>
      <div className={classes.inputDiv}>
        <input
          className={classes.input}
          type={props.type}
          id={props.id}
          onChange={inputChangeHandler}
          onBlur={inputBlurHandler}
          value={enteredInput}
        ></input>
      </div>
      <div className={classes.span}>
        {enteredInputIsInValid && <span>* Required</span>}
      </div>
    </div>
  );
};

export default Input;
