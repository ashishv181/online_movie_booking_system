import "./css/Signin.css";
import { useState, useContext } from "react";
import { url } from "../../Common/common";
import axios from "axios";
import AuthContext from "../../Common/auth-context";
import { useHistory } from "react-router";

const Signin = () => {
  const [userEmail, setUserEmail] = useState("");
  const [userPassword, setUserPassword] = useState("");
  const [isChecked, setIsChecked] = useState(false);
  const { user, setUser } = useContext(AuthContext);

  // changing the state of the clicked state depending on whether checkbox selected
  const checkHandler = () => {
    if (isChecked == false) {
      setIsChecked(true);
    } else {
      setIsChecked(false);
    }
  };

  console.log(isChecked);

  //calling userRequest if checkbox not clicked
  const userGetRequest = (data) => {
    axios.post(url + "/signin", data).then((response) => {
      const result = response.data;
      if (result.status == null) {
        alert("error signin in");
        // staffGetRequest();
      } else {
        alert("success");
        localStorage.setItem("myUser", JSON.stringify(result.status));
        setUser(result.status);
      }
    });
  };

  //calling staffrequest if checkbox is checked
  const staffGetRequest = (data) => {
    axios.post(url + "/staff/signin", data).then((response) => {
      const result = response.data;
      if (result.status == "error") {
        alert("error signing in");
      } else {
        alert("success");
        console.log(result.status);
        localStorage.setItem("myUser", JSON.stringify(result.status));
        localStorage.setItem("role", JSON.stringify(result.role));
        setUser(result.status);
      }
    });
  };

  const signinUser = () => {
    if (userEmail.trim() === "") {
      alert("please enter email");
    } else if (userPassword.trim() === "") {
      alert("please enter password");
    } else {
      const data = new FormData();
      data.append("email", userEmail);
      data.append("password", userPassword);

      {
        isChecked ? staffGetRequest(data) : userGetRequest(data);
      }
    }
  };

  return (
    <div className="container signInContainer">
      <span className="header">Sign in</span>

      <div className="mb-4">
        <label className="mb-4">Email</label>
        <input
          onChange={(event) => {
            setUserEmail(event.target.value);
          }}
          placeholder="enter your email"
          className="form-control"
          type="email"
        />
      </div>
      <div className="mb-4">
        <label className="mb-1">Password</label>
        <input
          onChange={(event) => {
            setUserPassword(event.target.value);
          }}
          placeholder="enter your password"
          className="form-control"
          type="password"
        />
      </div>
      <input onChange={checkHandler} type="checkbox" />
      <span>Check if Admin or Staff</span>
      <div className="mb-4">
        <button onClick={signinUser} className="btn btn-success">
          Signin
        </button>
      </div>
    </div>
  );
};

export default Signin;
