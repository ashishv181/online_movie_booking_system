import "./css/Signup.css";
import { useState } from "react";
import { useHistory, Link } from "react-router-dom";
import { url } from "../../Common/common";
import axios from "axios";

const Signup = () => {
  const [userName, setName] = useState("");
  const [userEmail, setEmail] = useState("");
  const [userPhone, setPhone] = useState("");
  const [userPassword, setPassword] = useState("");
  const [userdob, setdob] = useState(0);

  // const history = useHistory();

  const signupUser = () => {
    const data = new FormData();

    data.append("userName", userName);
    data.append("userEmail", userEmail);
    data.append("userPhone", userPhone);
    data.append("userPassword", userPassword);
    data.append("userdob", userdob);

    axios.post(url + "/signup", data).then((response) => {
      const result = response.data;
      if (result.status === "success") {
        alert("successfully added an user");
        setName("");
        setEmail("");
        setPhone("");
        setPassword("");
        setdob("");
        // history.push("/Signin");
      } else {
        alert("error while adding user");
      }
    });

   
  };

  return (
    <div className="container background signUp-Container">
      <span className="header">Sign Up</span>

      <div className="mb-3">
        <label>Name</label>
        <input
          onChange={(event) => {
            setName(event.target.value);
          }}
          className="form-control"
          type="text"
          value={userName}
        />
      </div>
      <div className="mb-3">
        <label>Email</label>
        <input
          onChange={(event) => {
            setEmail(event.target.value);
          }}
          className="form-control"
          type="text"
          value={userEmail}

        />
      </div>
      <div className="mb-3">
        <label>Phone</label>
        <input
          onChange={(event) => {
            setPhone(event.target.value);
          }}
          className="form-control"
          type="email"
          value={userPhone}

        />
      </div>
      <div className="mb-3">
        <label>Password</label>
        <input
          onChange={(event) => {
            setPassword(event.target.value);
          }}
          className="form-control"
          type="password"
          value={userPassword}

        />
      </div>
      <div className="mb-3">
        <label>Date Of Birth</label>
        <input
          onChange={(event) => {
            setdob(event.target.value);
          }}
          className="form-control"
          type="date"
          value={userdob}

        />
      </div>
      <div className="mb-3">
        <button onClick={signupUser} className="btn btn-success">
          Signup
        </button>
        {/* <Link to="/signin">
          <a className="btn btn-warning" href = "signin">Signin</a>
        </Link> */}
      </div>
    </div>
  );
};

export default Signup;
