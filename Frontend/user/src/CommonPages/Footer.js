import React from "react";
import classes from "./Footer.css";
import sunbeamLogo from "../Images/sunbeam.jfif";

const FooterPage = () => {
  return (
    <div className="mainFooterDiv">
      <span className="footerMesage">Made With Love @ Sunbeam</span>
      <div className="footerWrapper">
        {/* <img className="footerLogo" src={sunbeamLogo} alt="logo" /> */}
        <div className="footerTable">
          <div className="memberName"> Mohini Aher</div>
          <div className="memberRoll"> D1 - 49787</div>
          <div className="memberPhone"> 7517 687 673</div>
        </div>
        <div className="footerTable">
          <div className="memberName"> Akash Deulkar</div>
          <div className="memberRoll"> D1 - 49506</div>
          <div className="memberPhone"> 7020 233 411</div>
        </div>
        <div className="footerTable">
          <div className="memberName"> Ashutosh Bagdia</div>
          <div className="memberRoll"> D1 - 49508</div>
          <div className="memberPhone"> 8275 291 237</div>
        </div>
        <div className="footerTable">
          <div className="memberName"> Ashish Varghese</div>
          <div className="memberRoll"> D1 - 49605</div>
          <div className="memberPhone"> 9920 414 457</div>
        </div>
      </div>
    </div>
  );
};

export default FooterPage;
