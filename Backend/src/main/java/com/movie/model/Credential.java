package com.movie.model;

import com.movie.entities.User;

public class Credential {
	private String email;
	private String password;
	
	public Credential() {
		// TODO Auto-generated constructor stub
	}

	public Credential(String userEmail, String userPassword) {
		this.email = userEmail;
		this.password = userPassword;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String userEmail) {
		this.email = userEmail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String userPassword) {
		this.password = userPassword;
	}

	@Override
	public String toString() {
		return "Credential [userEmail=" + email + ", userPassword=" + password + "]";
	}
}
