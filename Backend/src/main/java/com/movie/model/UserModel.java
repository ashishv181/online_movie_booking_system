package com.movie.model;

public class UserModel {
	private String userName;
	private String userEmail;
	private String userPhone;
	private String userPassword;
	private String userdob;

	public UserModel() {
		// TODO Auto-generated constructor stub
	}

	public UserModel(String userName, String userEmail, String userPhone, String userPassword, String userdob) {
		this.userName = userName;
		this.userEmail = userEmail;
		this.userPhone = userPhone;
		this.userPassword = userPassword;
		this.userdob = userdob;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserdob() {
		return userdob;
	}

	public void setUserdob(String userdob) {
		this.userdob = userdob;
	}

	@Override
	public String toString() {
		return "UserModel [userName=" + userName + ", userEmail=" + userEmail + ", userPhone=" + userPhone
				+ ", userPassword=" + userPassword + ", userdob=" + userdob + "]";
	}
}
