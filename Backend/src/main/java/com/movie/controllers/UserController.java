package com.movie.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.movie.entities.User;
import com.movie.model.UserModel;
import com.movie.services.UserServicesImpl;

@CrossOrigin("*")
@RequestMapping("/user")
@Controller
public class UserController {

	@Autowired
	private UserServicesImpl userService;
	
//	@PutMapping("/edit")
//	public ResponseEntity<Map<String, String>> edit(UserModel modelUser, HttpSession session) {
//		User loggedUser = (User)session.getAttribute("loggedInUser");
//		loggedUser = userService.validateInputs(modelUser, loggedUser);
//		
//		User user = userService.save(loggedUser);
//		Map<String, String> map = new HashMap<>();
//		if (user == null) {
//			map.put("status", "error");
//		} else {
//			map.put("status", "success");
//		}
//		return ResponseEntity.ok(map);
//	}
	
	@PutMapping("/edit")
	public ResponseEntity<Map<String, User>> edit(User user) {
		User editedUser = userService.save(user);
		Map<String, User> map = new HashMap<>();
		if (editedUser == null) {
			map.put("status", null);
		} else {
			map.put("status", editedUser);
		}
		return ResponseEntity.ok(map);
	}
}
