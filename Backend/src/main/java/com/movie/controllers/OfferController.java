package com.movie.controllers;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.movie.entities.AppOffer;
import com.movie.services.OfferService;

@CrossOrigin("*")
@RequestMapping("/offer")
@Controller
public class OfferController {
	@Autowired
	private OfferService offerService;

	@PostMapping("")
	public ResponseEntity<Map<String, String>> addManager(AppOffer offer) {
		AppOffer offer1 = offerService.save(offer);
		Map<String, String> map = new HashMap<>();
		if (offer1 == null) {
			map.put("status", "error");
		} else {
			map.put("status", "success");
		}
		return ResponseEntity.ok(map);
	}

	@GetMapping("/getoffer")
	public ResponseEntity<List<AppOffer>> findAllOffers() {

		List<AppOffer> alloffers = offerService.findAll();
		return ResponseEntity.ok(alloffers);
	}

	/*
	 * Map<String,List<Manager>> map = new HashMap<>(); if(allmanagers == null) {
	 * map.put("status", null); }else { map.put("status", allmanagers); }
	 */

	@DeleteMapping("/{id}")
	public ResponseEntity<Map<String, String>> deleteOffer(@PathVariable("id") int id) {

		offerService.deleteById(id);
		Map<String, String> map = new HashMap<>();
		AppOffer offer1= offerService.getById(id);
		System.out.println("offer1: "+offer1);
		if (offer1 != null) {
			map.put("status", "error");
		} else {
			map.put("status", "success");
		}
		return ResponseEntity.ok(map);

	}

	@GetMapping("/{id}")
	public ResponseEntity<Map<String, AppOffer>> findbyid(@PathVariable("id") int id) {

		AppOffer offer1 = offerService.findById(id);
		Map<String, AppOffer> map = new HashMap<>();
		if (offer1 != null) {
			map.put("status", offer1);
		} else {
			map.put("status", null);
		}
		return ResponseEntity.ok(map);

	}
//	@PutMapping("/{id}")
//	public ResponseEntity<Map<String, String>> updateManager(@PathVariable("id") int id , Manager man) {
//
//		//Manager man1 = offerService.findById(id);
//		System.out.println(man);
//		Manager man2 = offerService.save(man);
//		Map<String, String> map = new HashMap<>();
//		
//		if (man2 == null) {
//			map.put("status", "error");
//		} else {
//			map.put("status", "success");
//		}
//		return ResponseEntity.ok(map);
//
//	}
}
