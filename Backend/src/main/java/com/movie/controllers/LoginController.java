package com.movie.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.movie.entities.Admin;
import com.movie.entities.Employee;
import com.movie.entities.Manager;
import com.movie.entities.User;
import com.movie.model.Credential;
import com.movie.services.AdminServiceImpl;
import com.movie.services.EmployeeServiceImpl;
import com.movie.services.ManagerServiceImpl;
import com.movie.services.UserServicesImpl;

@CrossOrigin("*")
@Controller
public class LoginController {

	@Autowired
	private UserServicesImpl userService;
	@Autowired
	private AdminServiceImpl adminService;
	@Autowired
	private EmployeeServiceImpl employeeService;
	@Autowired
	private ManagerServiceImpl managerService;

	@PostMapping("/signin")
	public ResponseEntity<Map<String,User>> signIn (Credential cred){
		System.out.println("cred: "+cred);
		User user = userService.authenticate(cred.getEmail(), cred.getPassword());
		System.out.println(user);
		Map<String, User> map = new HashMap<>();
		if(user == null) {
			map.put("status", null);
		}else {
			map.put("status", user);
		}
		return ResponseEntity.ok(map);
	}
	
	@PostMapping("/staff/signin")
	public ResponseEntity<Map<String, Object>> staffSignin(Credential cred){
		Map<String, Object> map = new HashMap<>();
		Admin admin = adminService.authenticate(cred);
		if(admin != null) {
			map.put("status", admin);
			map.put("role", "admin");
		}else {
			Manager manager = managerService.authenticate(cred);
			if(manager != null) {
				map.put("status", manager);
				map.put("role", "manager");
			}else {
				Employee emp = employeeService.authenticate(cred);
				if(emp != null) {
					map.put("status", emp);
					map.put("role", "employee");
				}else {
					map.put("status", "error");
				}
			}
		}
		return ResponseEntity.ok(map);
	}
	
	@PostMapping("/signup")
	public ResponseEntity<Map<String,String>> signUp(User us) {
		User user = userService.save(us);
		Map<String, String> map = new HashMap<>();
		if(user == null) {
			map.put("status", "error");
		}else {
			map.put("status", "success");
		}
		return ResponseEntity.ok(map);
	}
	
	@GetMapping("/{email}")
	public ResponseEntity<User> findByEmail(@PathVariable("email") String email) {
		User user = userService.findByUserEmail(email);
		if(user == null)
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		return ResponseEntity.ok(user);
	}
	
}

