package com.movie.controllers;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.movie.utils.DiskStorageImpl;

@CrossOrigin("*")
@RequestMapping("/image")
@Controller
public class FileControllerImpl {
		
	@GetMapping(value="/{fileName}", produces = "image/*")
	public void download(@PathVariable("fileName") String fileName, HttpServletResponse resp) {
		System.out.println("Loading file: "+ fileName);
		Resource resource = DiskStorageImpl.load(fileName );
		if(resource != null) {
			try(InputStream in = resource.getInputStream()){
				ServletOutputStream out = resp.getOutputStream();
				FileCopyUtils.copy(in, out);
			}catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
