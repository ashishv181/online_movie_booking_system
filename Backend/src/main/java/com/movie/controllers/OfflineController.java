package com.movie.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.movie.entities.OfflineBooking;
import com.movie.entities.OnlineBooking;
import com.movie.services.OfflineServiceImpl;

@CrossOrigin("*")
@Controller
@RequestMapping("/offlineBook")
public class OfflineController {
	
	@Autowired
	private OfflineServiceImpl offlineService;
	
	@PostMapping("/book")
	public ResponseEntity<Map<String, String>> bookShow (OfflineBooking book){
		System.out.println(book);
		OfflineBooking bk = offlineService.save(book);
		Map<String, String> map = new HashMap<>();
		if(bk == null) {
			map.put("status", "error");
		}else {
			map.put("status", "success");
		}
		return ResponseEntity.ok(map);
	}
}
