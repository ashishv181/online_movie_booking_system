package com.movie.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.movie.entities.Manager;
import com.movie.services.ManagerServiceImpl;

@CrossOrigin("*")
@RequestMapping("/manager")
@Controller
public class ManagerController {
	@Autowired
	private ManagerServiceImpl managerService;

	@PostMapping("/add")
	public ResponseEntity<Map<String, String>> addManager(Manager man) {
		Manager manager = managerService.save(man);
		Map<String, String> map = new HashMap<>();
		if (manager == null) {
			map.put("status", "error");
		} else {
			map.put("status", "success");
		}
		return ResponseEntity.ok(map);
	}

	@GetMapping("/getManagers")
	public ResponseEntity<List<Manager>> findAllManagers() {

		List<Manager> allmanagers = managerService.findAll();
		allmanagers.forEach(System.out::println);
		return ResponseEntity.ok(allmanagers);
	}

	/*
	 * Map<String,List<Manager>> map = new HashMap<>(); if(allmanagers == null) {
	 * map.put("status", null); }else { map.put("status", allmanagers); }
	 */

	@DeleteMapping("/{id}")
	public ResponseEntity<Map<String, String>> deleteManager(@PathVariable("id") int id) {

		managerService.deleteById(id);
		Map<String, String> map = new HashMap<>();
		Manager man = managerService.findById(id);
		if (man != null) {
			map.put("status", "error");
		} else {
			map.put("status", "success");
		}
		return ResponseEntity.ok(map);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Map<String, Manager>> findbyid(@PathVariable("id") int id) {

		Manager man = managerService.findById(id);
		Map<String, Manager> map = new HashMap<>();
		if (man != null) {
			map.put("status", man);
		} else {
			map.put("status", null);
		}
		return ResponseEntity.ok(map);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Map<String, String>> updateManager(@PathVariable("id") int id , Manager man) {

//		Manager man1 = managerService.findById(id);
		System.out.println(man);
		Manager man2 = managerService.save(man);
		Map<String, String> map = new HashMap<>();
		
		if (man2 == null) {
			map.put("status", "error");
		} else {
			map.put("status", "success");
		}
		return ResponseEntity.ok(map);
	}
	
}
