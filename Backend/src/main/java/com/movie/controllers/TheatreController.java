package com.movie.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.movie.entities.Movie;
import com.movie.entities.Show;
import com.movie.entities.Theatre;
import com.movie.services.TheatreServiceImpl;
import com.movie.utils.DiskStorageImpl;

@CrossOrigin("*")
@Controller
@RequestMapping("/theatre")
public class TheatreController {
	
	@Autowired
	private TheatreServiceImpl theatreService;
	
	@GetMapping("/{showId}")
	public ResponseEntity<List<Theatre>> findAllTheatreByShow(@PathVariable("showId") int id){
		List<Theatre> theatres = theatreService.findAllTheatresByShowId(id);
		return ResponseEntity.ok(theatres);
	}
	
	@GetMapping("/getTheatre/{id}")
	public ResponseEntity<Theatre> findTheatreById(@PathVariable("id") int id){
		Theatre theatre = theatreService.findById(id);
		if(theatre == null) {
			return ResponseEntity.ok(null);
		}else {
			return ResponseEntity.ok(theatre);
		}
	}
	
	@GetMapping("/")
	public ResponseEntity<List<Theatre>> getAllTheatres(){
		List<Theatre> alltheatres = theatreService.findAll();
		return ResponseEntity.ok(alltheatres);	
	}
	
	@PostMapping("/add")
	public ResponseEntity<String> addTheatre (Theatre theatre){
		Theatre thtr = theatreService.save(theatre);
		if(thtr == null) {
			return ResponseEntity.ok("error");
		}else {
			return ResponseEntity.ok("success");
		}
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<String> deleteMovie (@PathVariable("id") int id){
		Theatre theatre = theatreService.findById(id);
		theatreService.delete(theatre);
		Theatre thtr = theatreService.findById(id);
		if(thtr == null) {
			return ResponseEntity.ok("success");
		}else {
			return ResponseEntity.ok("error");
		}
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<String> updateMovie (@PathVariable("id") int id, Theatre theatre){
		System.out.println("id: "+id);
//		Theatre theatre1 = theatreService.findById(id);
//		System.out.println("theatre1: "+ theatre1);
//		System.out.println("theatre: "+ theatre);
		Theatre updated = theatreService.save(theatre);
		
		if(updated != null) {
			return ResponseEntity.ok("success");
		}else {
			return ResponseEntity.ok("error");
		}
	}
	
}
