package com.movie.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.movie.daos.MovieDao;
import com.movie.entities.FoodItem;
import com.movie.entities.Movie;
import com.movie.services.MovieServiceImpl;
import com.movie.utils.DiskStorageImpl;

@CrossOrigin("*")
@Controller
@RequestMapping("/movie")
public class MovieController {
	
	@Autowired
	private MovieServiceImpl movieService;
	
	@GetMapping("/getMovies")
	public ResponseEntity<Map<String, List<Movie>>> getAllMovies (){
		List<Movie> allMovies = movieService.findAll();
		Map<String, List<Movie>> map = new HashMap<>();
		if(allMovies == null) {
			map.put("status", null);
		}else {
			map.put("status", allMovies);
		}
		return ResponseEntity.ok(map);
	}
	
	@GetMapping("/info/{id}")
	public ResponseEntity<Map<String, Movie>> getMovie (@PathVariable("id") int id){
		System.out.println("id: "+id);
		Movie movie = movieService.findById(id);
		System.out.println(movie);
		Map<String, Movie> map = new HashMap<>();
		if(movie == null) {
			map.put("status", null);
		}else {
			map.put("status", movie);
		}
		return ResponseEntity.ok(map);
	}
	
	@PostMapping("/addMovie")
	public ResponseEntity<Map<String, String>> addMovie(Movie movie, @RequestParam("image") MultipartFile mpFile){
		String fileName = DiskStorageImpl.store(mpFile);
		movie.setMovieImage(fileName);
		Movie addedMovie = movieService.save(movie);
		Map<String, String> map = new HashMap<>();
		if(addedMovie == null) {
			map.put("status", "error");
		}else {
			map.put("status", "success");
		}
		return ResponseEntity.ok(map);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<String> deleteMovie (@PathVariable("id") int id){
		Movie movie = movieService.findById(id);
		movieService.delete(movie);
		Movie check = movieService.findById(id);
		if(check == null) {
			return ResponseEntity.ok("success");
		}else {
			return ResponseEntity.ok("error");
		}
	}
	
	@PutMapping("/update")
	public ResponseEntity<String> updateMovie (Movie movie, @RequestParam("image") MultipartFile mpFile){
		String fileName = DiskStorageImpl.store(mpFile);
		movie.setMovieImage(fileName);
		Movie updated = movieService.save(movie);
		
		if(updated != null) {
			return ResponseEntity.ok("success");
		}else {
			return ResponseEntity.ok("error");
		}
	}
	
}
