package com.movie.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.movie.entities.Employee;
import com.movie.entities.FoodItem;
import com.movie.entities.Movie;
import com.movie.services.EmployeeServiceImpl;
import com.movie.services.FoodServiceImpl;
import com.movie.utils.DiskStorageImpl;


@CrossOrigin("*")
@RequestMapping("/employee")
@Controller
public class EmployeeController {
	
	@Autowired
	private EmployeeServiceImpl empService;
	@Autowired
	private FoodServiceImpl foodService;
	
	@GetMapping("/foodItems")
	public ResponseEntity<Map<String, List<FoodItem>>> getAllFoodItems(){
		List<FoodItem> allFoodItems = foodService.findAll();
		Map<String, List<FoodItem>> map = new HashMap<>();
		if(allFoodItems == null) {
			map.put("status", null);
		}else {
			map.put("status", allFoodItems);
		}
		return ResponseEntity.ok(map);
	}
	
	@PostMapping("/addFoodItem")
	public ResponseEntity<Map<String, String>> addFoodItem(FoodItem item, @RequestParam("image") MultipartFile mpFile){
		String fileName = DiskStorageImpl.store(mpFile);
		item.setFoodItemImage(fileName);
		FoodItem savedItem = foodService.save(item);
		Map<String, String> map = new HashMap<>();
		if(savedItem == null) {
			map.put("status", "error");
		}else {
			map.put("status", savedItem.toString());
		}		
		return ResponseEntity.ok(map);
	}
	
	@GetMapping("/foodItem/{name}")
	public ResponseEntity<Map<String, FoodItem>> getFoodItem(@PathVariable("name") String name){
		FoodItem food = foodService.findByName(name);
		Map<String, FoodItem> map = new HashMap<>();
		if(food == null) {
			map.put("status", null);
		}else {
			map.put("status", food);
		}
		return ResponseEntity.ok(map);
	}
	
	@GetMapping("/getEmployees")
	public ResponseEntity<List<Employee>> getAllEmps(){
		List<Employee> allEmp = empService.findAll();
		return ResponseEntity.ok(allEmp);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Employee> updateMovie (@PathVariable("id") int id){
		Employee emp = empService.findById(id);
		
		if(emp != null) {
			return ResponseEntity.ok(emp);
		}else {
			return ResponseEntity.ok(null);
		}
	}
	
	@PostMapping("/addEmployee")
	public ResponseEntity<Employee> addFoodItem(Employee emp){

		Employee savedItem = empService.save(emp);
		
		return ResponseEntity.ok(savedItem);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<String> addFoodItem(@PathVariable("id") int id){
		Employee emp = empService.findById(id);
		empService.delete(emp);
		Employee emp2 = empService.findById(id);
		if(emp2 == null) {
			return ResponseEntity.ok("success");
		}else {
			return ResponseEntity.ok("error");
		}
	}
	
	@PutMapping("/update")
	public ResponseEntity<String> updateMovie (Employee emp){
		Employee updatedEmp = empService.save(emp);
		
		if(updatedEmp != null) {
			return ResponseEntity.ok("success");
		}else {
			return ResponseEntity.ok("error");
		}
	}
	
}
