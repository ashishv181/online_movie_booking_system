package com.movie.controllers;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.movie.entities.Show;

import com.movie.services.ShowService;

@CrossOrigin("*")
@RequestMapping("/show")
@Controller
public class ShowController {
	@Autowired
	private ShowService showService;

	@PostMapping("")
	public ResponseEntity<Map<String, String>> addShow(Show show1) {
		System.out.println(show1);
		Show show = showService.save(show1);
		Map<String, String> map = new HashMap<>();
		if (show == null) {
			map.put("status", "error");
		} else {
			map.put("status", "success");
		}
		return ResponseEntity.ok(map);
	}

	@GetMapping("/getShow")
	public ResponseEntity<List<Show>> findAllShows() {

		List<Show> allshows = showService.findAll();
		return ResponseEntity.ok(allshows);
	}
	
	@GetMapping("/getShow/{movieId}")
	public ResponseEntity<List<Show>> findAllShowsOfAMovie(@PathVariable("movieId") int id) {
		List<Show> allshows = showService.findAllShowByMovieId(id);
		allshows.forEach(System.out::println);
		return ResponseEntity.ok(allshows);
	}
	
	@GetMapping("/getShowByTheatre/{theatreId}")
	public ResponseEntity<List<Show>> findAllShowsOfTheatre(@PathVariable("theatreId") int id) {
		List<Show> allshows = showService.findAllShowByTheatreId(id);
		allshows.forEach(System.out::println);
		return ResponseEntity.ok(allshows);
	}

	/*
	 * Map<String,List<Show>> map = new HashMap<>(); if(allshows == null) {
	 * map.put("status", null); }else { map.put("status", allshows); }
	 */

	@DeleteMapping("/{id}")
	public ResponseEntity<Map<String, String>> deleteShow(@PathVariable("id") int id) {

		showService.deleteById(id);
		Map<String, String> map = new HashMap<>();
		Show show = showService.findById(id);
		if (show != null) {
			map.put("status", "error");
		} else {
			map.put("status", "success");
		}
		return ResponseEntity.ok(map);

	}

	@GetMapping("/{id}")
	public ResponseEntity<Map<String, Show>> findbyid(@PathVariable("id") int id) {

		Show show = showService.findById(id);
		Map<String, Show> map = new HashMap<>();
		if (show != null) {
			map.put("status", show);
		} else {
			map.put("status", null);
		}
		return ResponseEntity.ok(map);
 
	}
	@PutMapping("/")
	public ResponseEntity<Map<String, String>> updateShow(Show show) {

		System.out.println(show);
		Show show2 = showService.save(show);
		Map<String, String> map = new HashMap<>();
		
		if (show2 == null) {
			map.put("status", "error");
		} else {
			map.put("status", "success");
		}
		return ResponseEntity.ok(map);

	}
	
	@PutMapping("/bookShow/{id}")
	public ResponseEntity<Map<String, Show>> updateShowAfterBook(@PathVariable("id") int id ,Show show) {
		System.out.println(id);
		int num = show.getNoOfSeatsLeft();
		System.out.println("Number left: " +num);
		
		Show show1 = showService.findById(id);
		show1.setNoOfSeatsLeft(num);
//		System.out.println(show);
		Show show2 = showService.save(show1);
		Map<String, Show> map = new HashMap<>();
		
		if (show2 == null) {
			map.put("status", null);
		} else {
			map.put("status", show2);
		}
		return ResponseEntity.ok(map);
	}
	
	
	
}
