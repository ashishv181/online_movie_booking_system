package com.movie.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.movie.entities.OnlineBooking;
import com.movie.services.OnlineBookingServiceImpl;

@CrossOrigin("*")
@Controller
@RequestMapping("/book")
public class BookingController {

	@Autowired
	private OnlineBookingServiceImpl onlineBookingService;
	
	@PostMapping("/")
	public ResponseEntity<Map<String, String>> bookShow (OnlineBooking book){
		OnlineBooking bk = onlineBookingService.save(book);
		Map<String, String> map = new HashMap<>();
		if(bk == null) {
			map.put("status", "error");
		}else {
			map.put("status", "success");
		}
		return ResponseEntity.ok(map);
	}
}
