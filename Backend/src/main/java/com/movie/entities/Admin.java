package com.movie.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/*CREATE TABLE admin_signup
(admin_id INT Primary key AUTO_INCREMENT ,
admin_name varchar(100),
admin_mobile Varchar(20),
admin_email varchar(100),
admin_password varchar(200),
created_on timestamp DEFAULT CURRENT_TIMESTAMP); */

@Entity
@Table(name="admin_signup")
public class Admin  {
	@Id
	@Column(name="admin_id")
	private int adminId;
	@Column(name="admin_name")
	private String adminName;
	@Column(name="admin_mobile")
	private String adminMobile;
	@Column(name="admin_email")
	private String adminEmail;
	@Column(name="admin_password")
	private String adminPassword;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="admin_dob")
	private Date adminDob;
	public Date getAdminDob() {
		return adminDob;
	}

	public void setAdminDob(Date adminDob) {
		this.adminDob = adminDob;
	}

	@Column(name = "created_on", insertable = false, updatable = false)
	private Date createdTimeStamp;
	
	public Admin() {
		// TODO Auto-generated constructor stub
	}

	public int getAdminId() {
		return adminId;
	}

	public void setAdminId(int adminId) {
		this.adminId = adminId;
	}

	public String getAdminName() {
		return adminName;
	}

	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}

	public String getAdminMobile() {
		return adminMobile;
	}

	public void setAdminMobile(String adminMobile) {
		this.adminMobile = adminMobile;
	}

	public String getAdminEmail() {
		return adminEmail;
	}

	public void setAdminEmail(String adminEmail) {
		this.adminEmail = adminEmail;
	}

	public String getAdminPassword() {
		return adminPassword;
	}

	public void setAdminPassword(String adminPassword) {
		this.adminPassword = adminPassword;
	}

	public Date getCreatedTimeStamp() {
		return createdTimeStamp;
	}

	public void setCreatedTimeStamp(Date createdTimeStamp) {
		this.createdTimeStamp = createdTimeStamp;
	}

	@Override
	public String toString() {
		return "Admin [adminId=" + adminId + ", adminName=" + adminName + ", adminMobile=" + adminMobile
				+ ", adminEmail=" + adminEmail + ", adminPassword=" + adminPassword + ", adminDob=" + adminDob
				+ ", createdTimeStamp=" + createdTimeStamp + "]";
	}
	
	
}
