package com.movie.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/*CREATE table food_item(
    food_item_id INT primary key AUTO_INCREMENT,
    food_item_name varchar(50),
    food_item_price INT
);*/
@Entity
@Table(name = "food_item")
public class FoodItem {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "food_item_id")
	private int foodId;
	@Column(name = "food_item_name")
	private String foodItemName;
	@Column(name="food_image")
	private String foodItemImage;
	@Column(name = "food_item_price")
	private int foodItemPrice;

	public FoodItem() {
		// TODO Auto-generated constructor stub
	}

	public int getFoodId() {
		return foodId;
	}

	public void setFoodId(int foodId) {
		this.foodId = foodId;
	}

	public String getFoodItemName() {
		return foodItemName;
	}

	public void setFoodItemName(String foodItemName) {
		this.foodItemName = foodItemName;
	}

	public int getFoodItemPrice() {
		return foodItemPrice;
	}

	public void setFoodItemPrice(int foodItemPrice) {
		this.foodItemPrice = foodItemPrice;
	}

	public String getFoodItemImage() {
		return foodItemImage;
	}

	public void setFoodItemImage(String foodItemImage) {
		this.foodItemImage = foodItemImage;
	}

	@Override
	public String toString() {
		return "FoodItem [foodId=" + foodId + ", foodItemName=" + foodItemName + ", foodItemImage=" + foodItemImage
				+ ", foodItemPrice=" + foodItemPrice + "]";
	}


	
}
