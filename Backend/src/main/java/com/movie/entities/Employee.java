package com.movie.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

/*CREATE TABLE employee_signup
(employee_id INT AUTO_INCREMENT,
employee_name varchar(100),
employee_mobile Varchar(20),
employee_email varchar(100),
employee_password varchar(200),
employee_dob Date,
theatre_id INT,
created_on timestamp DEFAULT CURRENT_TIMESTAMP,
Primary key(employee_id),
FOREIGN key (theatre_id) references theatre(theatre_id));*/

@Entity
@Table(name="employee_signup")
public class Employee {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="employee_id")
	private int employeeId;
	@Column(name="employee_name")
	private String employeeName;
	@Column(name="employee_mobile")
	private String employeeMobile;
	@Column(name="employee_email")
	private String employeeEmail;
	@Column(name="employee_password")
	private String employeePassword;
	@Column(name="employee_dob")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date employeeDob;
	@ManyToOne
	@JoinColumn(name="theatre_id")
	private Theatre theatre;
	
	public Employee() {
		
	}

	public Employee(int employeeId, String employeeName, String employeeMoblie, String employeeEmail,
			String employeePassword, Date employeeDob, Theatre theatre) {
		super();
		this.employeeId = employeeId;
		this.employeeName = employeeName;
		this.employeeMobile = employeeMoblie;
		this.employeeEmail = employeeEmail;
		this.employeePassword = employeePassword;
		this.employeeDob = employeeDob;
		this.theatre = theatre;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getEmployeeMobile() {
		return employeeMobile;
	}

	public void setEmployeeMobile(String employeeMoblie) {
		this.employeeMobile = employeeMoblie;
	}

	public String getEmployeeEmail() {
		return employeeEmail;
	}

	public void setEmployeeEmail(String employeeEmail) {
		this.employeeEmail = employeeEmail;
	}

	public String getEmployeePassword() {
		return employeePassword;
	}

	public void setEmployeePassword(String employeePassword) {
		this.employeePassword = employeePassword;
	}

	public Date getEmployeeDob() {
		return employeeDob;
	}

	public void setEmployeeDob(Date employeeDob) {
		this.employeeDob = employeeDob;
	}

	public Theatre getTheatre() {
		return theatre;
	}

	public void setTheatre(Theatre theatre) {
		this.theatre = theatre;
	}

	@Override
	public String toString() {
		return "Employee [employeeId=" + employeeId + ", employeeName=" + employeeName + ", employeeMoblie="
				+ employeeMobile + ", employeeEmail=" + employeeEmail + ", employeePassword=" + employeePassword
				+ ", employeeDob=" + employeeDob + ", theatre=" + theatre + "]";
	}

	
	
}
