	package com.movie.entities;
/*CREATE TABLE theatre(
    theatre_id INT primary key AUTO_INCREMENT,
    theatre_name varchar(100),
    theatre_address varchar(100),
    theatre_city varchar(100)); */

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="theatre")
public class Theatre {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="theatre_id")
	private int theatreId;
	@Column(name="theatre_name")
	private String theatreName;
	@Column(name="theatre_address")
	private String theatreAddress;
	@Column(name="theatre_city")
	private String theatreCity;
//	@OneToMany(mappedBy = "theatre")
//	private List<Employee> empList;
	public Theatre() {

	}
	public Theatre(int theatreId, String theatreName, String theatreAddress, String theatreCity
			/*List<Movie> movieList*/) {
		super();
		this.theatreId = theatreId;
		this.theatreName = theatreName;
		this.theatreAddress = theatreAddress;
		this.theatreCity = theatreCity;
//		this.empList = empList;
		/*this.movieList = movieList;*/
	}
	public int getTheatreId() {
		return theatreId;
	}
	public void setTheatreId(int theatreId) {
		this.theatreId = theatreId;
	}
	public String getTheatreName() {
		return theatreName;
	}
	public void setTheatreName(String theatreName) {
		this.theatreName = theatreName;
	}
	public String getTheatreAddress() {
		return theatreAddress;
	}
	public void setTheatreAddress(String theatreAddress) {
		this.theatreAddress = theatreAddress;
	}
	public String getTheatreCity() {
		return theatreCity;
	}
	public void setTheatreCity(String theatreCity) {
		this.theatreCity = theatreCity;
	}
//	public List<Employee> getEmpList() {
//		return empList;
//	}
//	public void setEmpList(List<Employee> empList) {
//		this.empList = empList;
//	}
//	public List<Movie> getMovieList() {
//		return movieList;
//	}
//	public void setMovieList(List<Movie> movieList) {
//		this.movieList = movieList;
//	}
	
	public String toString() {
//		return "Theatre [theatreId=" + theatreId + ", theatreName=" + theatreName + ", theatreAddress=" + theatreAddress
//				+ ", theatreCity=" + theatreCity + ", empList=" + empList + ", movieList=" + movieList + "]";
		return "Theatre [theatreId=" + theatreId + ", theatreName=" + theatreName + ", theatreAddress=" + theatreAddress
				+ ", theatreCity=" + theatreCity + "]";
	}

	

}
