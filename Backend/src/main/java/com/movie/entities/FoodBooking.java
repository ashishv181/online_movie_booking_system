package com.movie.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "food_book")
public class FoodBooking {
	@Id
	@Column(name = "food_booking_id")
	private int foodBookingId;
	@Column(name = "food_booking_payment_amount")
	private int foodBookingPaymentAmount;
//	@Column(name = "food_booking_quantity")
//	private int foodBookingQuantity;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "food_booking_date", insertable = false, updatable = false)
	private Date foodBookingDate;

	public FoodBooking() {
		// TODO Auto-generated constructor stub
	}

	public FoodBooking(int foodBookingId, int foodBookingPaymentAmount, Date foodBookingDate) {
		this.foodBookingId = foodBookingId;
		this.foodBookingPaymentAmount = foodBookingPaymentAmount;
		this.foodBookingDate = foodBookingDate;
	}

	public int getFoodBookingId() {
		return foodBookingId;
	}

	public void setFoodBookingId(int foodBookingId) {
		this.foodBookingId = foodBookingId;
	}

	public int getFoodBookingPaymentAmount() {
		return foodBookingPaymentAmount;
	}

	public void setFoodBookingPaymentAmount(int foodBookingPaymentAmount) {
		this.foodBookingPaymentAmount = foodBookingPaymentAmount;
	}

	public Date getFoodBookingDate() {
		return foodBookingDate;
	}

	public void setFoodBookingDate(Date foodBookingDate) {
		this.foodBookingDate = foodBookingDate;
	}

	@Override
	public String toString() {
		return "FoodBooking [foodBookingId=" + foodBookingId + ", foodBookingPaymentAmount=" + foodBookingPaymentAmount
				+ ", foodBookingDate=" + foodBookingDate + "]";
	}

}