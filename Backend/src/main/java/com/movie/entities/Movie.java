package com.movie.entities;

import java.sql.Time;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/*CREATE TABLE movie
(movie_id INT primary key AUTO_INCREMENT,
movie_name varchar(200),
movie_image varchar(300),
movie_description varchar(1000),
movie_runtime Time,
movie_age_limit INT);*/
@Entity
@Table(name = "movie")
public class Movie {
	@Id
	@Column(name = "movie_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int movieId;
	@Column(name = "movie_name")
	private String movieName;
	@Column(name = "movie_image")
	private String movieImage;
	@Column(name="movie_trailer")
	private String movieTrailer;
	@Column(name = "movie_description")
	private String movieDescription;
	@Column(name = "movie_runtime")
	// time type
//	@Temporal(TemporalType.TIME)
	private String movieRuntime;
	@Column(name = "movie_age_limit")
	private int ageLimit;
	
	@OneToMany(mappedBy = "movie", cascade = CascadeType.ALL)
	private List<Show> shows;

	public Movie() {

	}

	public Movie(int movieId, String movieName, String movieImage, String movieDescription, String movieRuntime,
			int ageLimit) {
		super();
		this.movieId = movieId;
		this.movieName = movieName;
		this.movieImage = movieImage;
		this.movieDescription = movieDescription;
		this.movieRuntime = movieRuntime;
		this.ageLimit = ageLimit;
	}

	public int getMovieId() {
		return movieId;
	}

	public void setMovieId(int movieId) {
		this.movieId = movieId;
	}

	public String getMovieName() {
		return movieName;
	}

	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}

	public String getMovieImage() {
		return movieImage;
	}

	public void setMovieImage(String movieImage) {
		this.movieImage = movieImage;
	}

	public String getMovieDescription() {
		return movieDescription;
	}

	public void setMovieDescription(String movieDescription) {
		this.movieDescription = movieDescription;
	}

	public String getMovieRuntime() {
		return movieRuntime;
	}

	public void setMovieRuntime(String movieRuntime) {
		this.movieRuntime = movieRuntime;
	}
	
	public String getMovieTrailer() {
		return movieTrailer;
	}

	public void setMovieTrailer(String movieTrailer) {
		this.movieTrailer = movieTrailer;
	}

	public int getAgeLimit() {
		return ageLimit;
	}

	public void setAgeLimit(int ageLimit) {
		this.ageLimit = ageLimit;
	}

	@Override
	public String toString() {
		return "Movie [movieId=" + movieId + ", movieName=" + movieName + ", movieImage=" + movieImage
				+ ", movieDescription=" + movieDescription + ", movieRuntime=" + movieRuntime + ", ageLimit=" + ageLimit
				+ "]";
	}

}
