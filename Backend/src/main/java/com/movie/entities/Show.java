package com.movie.entities;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonBackReference;

/*CREATE TABLE movie_show
(show_id INT AUTO_INCREMENT,
show_number INT,
show_start_time datetime,
show_end_time datetime,
movie_id INT,
no_of_seats_left INT,
show_revenue INT,
theatre_id INT,
screen_id INT,
Primary key(show_id),
FOREIGN key(theatre_id) references theatre(theatre_id),
FOREIGN key(screen_id) references screen(screen_id),
FOREIGN key(movie_id) references movie(movie_id));*/
@Entity
@Table(name = "movie_show")
public class Show {
	@Id
	@Column(name = "show_id")
	private int showId;
	@Column(name = "show_number")
	private int showNumber;
	@Column(name = "show_start_time")
//@DateTimeFormat(pattern ="yyyy-MM-dd HH:MI:SS")
//@Temporal(TemporalType.TIMESTAMP)
	private String showStartTime;
	@Column(name = "show_end_time")
//@DateTimeFormat(pattern ="yyyy-MM-dd HH:MI:SS")
//@Temporal(TemporalType.TIMESTAMP)
	private String showEndTime;
	@Column(name = "no_of_seats_left")
	private int noOfSeatsLeft;
	@Column(name = "show_revenue")
	private int showRevenue;
	@ManyToOne
	@JoinColumn(name = "theatre_id")
//	@JsonBackReference
	private Theatre theatre;

//	@ManyToOne
//	@JoinColumn(name = "screen_id")
//	@JsonBackReference
//	private Screen screen;
	@ManyToOne
	@JoinColumn(name = "movie_id")
//	@JsonBackReference
	private Movie movie;

	public Show() {

	}

	public Show(int showId, int showNumber, String showStartTime, String showEndTime, int noOfSeatsLeft,
			int showRevenue, Theatre theatre, Movie movie) {
		super();
		this.showId = showId;
		this.showNumber = showNumber;
		this.noOfSeatsLeft = noOfSeatsLeft;
		this.showRevenue = showRevenue;
		this.theatre = theatre;
		this.movie = movie;
	}

	public int getShowId() {
		return showId;
	}

	public void setShowId(int showId) {
		this.showId = showId;
	}

	public int getShowNumber() {
		return showNumber;
	}

	public void setShowNumber(int showNumber) {
		this.showNumber = showNumber;
	}

	public String getShowStartTime() {
		return showStartTime;
	}

	public void setShowStartTime(String showStartTime) {
		this.showStartTime = showStartTime;
	}

	public String getShowEndTime() {
		return showEndTime;
	}

	public void setShowEndTime(String showEndTime) {
		this.showEndTime = showEndTime;
	}

	public int getNoOfSeatsLeft() {
		return noOfSeatsLeft;
	}

	public void setNoOfSeatsLeft(int noOfSeatsLeft) {
		this.noOfSeatsLeft = noOfSeatsLeft;
	}

	public int getShowRevenue() {
		return showRevenue;
	}

	public void setShowRevenue(int showRevenue) {
		this.showRevenue = showRevenue;
	}

	public Theatre getTheatre() {
		return theatre;
	}

	public void setTheatre(Theatre theatre) {
		this.theatre = theatre;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	@Override
	public String toString() {
		return "Show [showId=" + showId + ", showNumber=" + showNumber + ", noOfSeatsLeft=" + noOfSeatsLeft
				+ ", showRevenue=" + showRevenue + ", theatre=" + theatre + ", movie=" + movie + "]";
	}

}
