package com.movie.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/*CREATE TABLE app_offer
(offer_id INT primary key AUTO_INCREMENT,
offer_name varchar(100),
offer_details varchar(100));*/
@Entity
@Table(name = "app_offer")
public class AppOffer {
	@Id
	@Column(name = "offer_id")
	private int offerId;
	@Column(name = "offer_name")
	private String offerName;
	@Column(name = "offer_details")
	private String offerDetails;

	public AppOffer() {

	}

	public AppOffer(int offerId, String offerName, String offerDetails) {
		super();
		this.offerId = offerId;
		this.offerName = offerName;
		this.offerDetails = offerDetails;
	}

	public int getOfferId() {
		return offerId;
	}

	public void setOfferId(int offerId) {
		this.offerId = offerId;
	}

	public String getOfferName() {
		return offerName;
	}

	public void setOfferName(String offerName) {
		this.offerName = offerName;
	}

	public String getOfferDetails() {
		return offerDetails;
	}

	public void setOfferDetails(String offerDetails) {
		this.offerDetails = offerDetails;
	}

	@Override
	public String toString() {
		return "AppOffer [offerId=" + offerId + ", offerName=" + offerName + ", offerDetails=" + offerDetails + "]";
	}

}
