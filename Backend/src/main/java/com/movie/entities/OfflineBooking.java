package com.movie.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "offline_booking")
public class OfflineBooking {
	@Id
	@Column(name = "offline_booking_id")
	private int bookingId;
	@OneToOne
	@JoinColumn(name = "show_id")
	private Show show;
	@Column(name = "offline_booking_date", insertable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date bookingDate;
	@Column(name = "no_of_seats_booked")
	private int seatsBooked;
	@Column(name = "offline_booking_amount")
	private int bookingAmount;
	@Column(name = "offline_booking_mobile")
	private String userMobile;

	public OfflineBooking() {
		// TODO Auto-generated constructor stub
	}

	public OfflineBooking(int bookingId, Show show, Date bookingDate, int seatsBooked, int bookingAmount,
			String userMobile) {
		this.bookingId = bookingId;
		this.show = show;
		this.bookingDate = bookingDate;
		this.seatsBooked = seatsBooked;
		this.bookingAmount = bookingAmount;
		this.userMobile = userMobile;
	}

	public int getBookingId() {
		return bookingId;
	}

	public void setBookingId(int bookingId) {
		this.bookingId = bookingId;
	}

	public Show getShow() {
		return show;
	}

	public void setShow(Show show) {
		this.show = show;
	}

	public Date getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}

	public int getSeatsBooked() {
		return seatsBooked;
	}

	public void setSeatsBooked(int seatsBooked) {
		this.seatsBooked = seatsBooked;
	}

	public int getBookingAmount() {
		return bookingAmount;
	}

	public void setBookingAmount(int bookingAmount) {
		this.bookingAmount = bookingAmount;
	}

	public String getUserMobile() {
		return userMobile;
	}

	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}

	@Override
	public String toString() {
		return "OfflineBooking [bookingId=" + bookingId + ", show=" + show + ", bookingDate=" + bookingDate
				+ ", seatsBooked=" + seatsBooked + ", bookingAmount=" + bookingAmount + ", userMobile=" + userMobile
				+ "]";
	}

}