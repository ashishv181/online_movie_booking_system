package com.movie.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="ticket_booking")
public class OnlineBooking {
	@Id
	@Column(name="ticket_booking_id")
	private int bookingId;
	@OneToOne
	@JoinColumn(name="show_id")
	private Show show;
	@Column(name="ticket_booking_date", insertable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date bookingDate;
	@Column(name="no_of_seats_booked")
	private int seatsBooked;
	@Column(name="ticket_booking_amount")
	private int bookingAmount;
	@ManyToOne
	@JoinColumn(name="user_id")
	private User user;
	
	public OnlineBooking() {
		// TODO Auto-generated constructor stub
	}

	public int getBookingId() {
		return bookingId;
	}

	public void setBookingId(int bookingId) {
		this.bookingId = bookingId;
	}

	public Show getShow() {
		return show;
	}

	public void setShow(Show show) {
		this.show = show;
	}

	public Date getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}

	public int getSeatsBooked() {
		return seatsBooked;
	}

	public void setSeatsBooked(int seatsBooked) {
		this.seatsBooked = seatsBooked;
	}

	public int getBookingAmount() {
		return bookingAmount;
	}

	public void setBookingAmount(int bookingAmount) {
		this.bookingAmount = bookingAmount;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "OnlineBooking [bookingId=" + bookingId + ", show=" + show + ", bookingDate=" + bookingDate
				+ ", seatsBooked=" + seatsBooked + ", bookingAmount=" + bookingAmount + ", user=" + user + "]";
	}
	
	
	
}	
