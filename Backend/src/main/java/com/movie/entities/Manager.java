package com.movie.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

/*CREATE TABLE manager_signup
(manager_id INT AUTO_INCREMENT,
manager_name varchar(100),
manager_mobile Varchar(20),
manager_email varchar(100),
manager_password varchar(200),
manager_dob Date,
theatre_id INT,
created_on timestamp DEFAULT CURRENT_TIMESTAMP,
Primary key(manager_id), 
FOREIGN key(theatre_id) references theatre(theatre_id));*/

@Entity
@Table(name="manager_signup")
public class Manager {
	@Id
	@Column(name="manager_id")
	private int managerId;
	@Column(name="manager_name")
	private String managerName;
	@Column(name="manager_mobile")
	private String managerMobile;
	@Column(name="manager_email")
	private String managerEmail;
	@Column(name="manager_password")
	private String managerPassword;
	@Column(name="manager_dob")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date managerDob;
	@OneToOne
	@JoinColumn(name="theatre_id")
	private Theatre theatre;
	
	public Manager() {
	
	}

	public int getManagerId() {
		return managerId;
	}

	public void setManagerId(int managerId) {
		this.managerId = managerId;
	}

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	public String getManagerMoblie() {
		return managerMobile;
	}

	public void setManagerMobile(String managerMoblie) {
		this.managerMobile = managerMoblie;
	}

	public String getManagerEmail() {
		return managerEmail;
	}

	public void setManagerEmail(String managerEmail) {
		this.managerEmail = managerEmail;
	}

	public String getManagerPassword() {
		return managerPassword;
	}

	public void setManagerPassword(String managerPassword) {
		this.managerPassword = managerPassword;
	}

	public Date getManagerDob() {
		return managerDob;
	}

	public void setManagerDob(Date managerDob) {
		this.managerDob = managerDob;
	}

	public Theatre getTheatre() {
		return theatre;
	}

	public void setTheatre(Theatre theatre) {
		this.theatre = theatre;
	}

	@Override
	public String toString() {
		return "Manager [managerId=" + managerId + ", managerName=" + managerName + ", managerMoblie=" + managerMobile
				+ ", managerEmail=" + managerEmail + ", managerPassword=" + managerPassword + ", managerDob="
				+ managerDob + ", theatre=" + theatre + "]";
	}
	

}
