package com.movie.entities;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;


@Entity
@Table(name ="user_signup")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name ="user_id")
	private int userId; 
	@Column(name ="user_name")
	private String userName;
	@Column(name ="user_email")
	private String userEmail;
	@Column(name ="user_phone")
	private String userPhone;
	@Column(name ="user_password")
	private String userPassword;
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name ="user_dob")
	private Date userdob;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_on", insertable = false, updatable = false)
	private Date createdTimestamp;
	
	@OneToMany(mappedBy = "user")
	private List<OnlineBooking> bookingList; 

	public User() {
	}

	public User(int userId, String userName, String userEmail, String userPhone, String userPassword, Date userdob,
			Date createdTimestamp) {
		this.userId = userId;
		this.userName = userName;
		this.userEmail = userEmail;
		this.userPhone = userPhone;
		this.userPassword = userPassword;
		this.userdob = userdob;
		this.createdTimestamp = createdTimestamp;
	}

	public int getUserId() {
		return userId;
	}

	public String getUserName() {
		return userName;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public Date getUserdob() {
		return userdob;
	}

	public Date getCreatedTimestamp() {
		return createdTimestamp;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public void setUserdob(Date userdob) {
		this.userdob = userdob;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", userName=" + userName + ", userEmail=" + userEmail + ", userPhone="
				+ userPhone + ", userPassword=" + userPassword + ", userdob=" + userdob + ", createdTimestamp="
				+ createdTimestamp + "]";
	}

	
	
}
