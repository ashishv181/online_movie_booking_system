package com.movie.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.movie.entities.Admin;

public interface AdminDao extends JpaRepository<Admin, Integer>{

	Admin findByadminEmail(String email);
}
