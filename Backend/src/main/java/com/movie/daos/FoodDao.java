package com.movie.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.movie.entities.FoodItem;

public interface FoodDao extends JpaRepository<FoodItem, Integer>{

	FoodItem findByFoodItemName(String name);

}
