package com.movie.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.movie.entities.Manager;

public interface ManagerDao extends JpaRepository<Manager, Integer> {
	
	Manager findBymanagerEmail(String email);

}
