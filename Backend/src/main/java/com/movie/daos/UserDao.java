package com.movie.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.movie.entities.User;

public interface UserDao extends JpaRepository<User, Integer>{

	User findByUserEmail(String email);
	
}
