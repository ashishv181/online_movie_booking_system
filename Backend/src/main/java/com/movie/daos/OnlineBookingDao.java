package com.movie.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.movie.entities.OnlineBooking;

public interface OnlineBookingDao extends JpaRepository<OnlineBooking, Integer> {

}
