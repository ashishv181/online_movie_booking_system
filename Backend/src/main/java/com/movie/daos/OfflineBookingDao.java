package com.movie.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.movie.entities.OfflineBooking;

public interface OfflineBookingDao extends JpaRepository<OfflineBooking, Integer> {

}
