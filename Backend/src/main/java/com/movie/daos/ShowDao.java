package com.movie.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.movie.entities.Show;

public interface ShowDao extends JpaRepository<Show, Integer>{

	@Query(value = "Select * from movie_show s where s.movie_id = :id", nativeQuery = true)
	List<Show> findAllByMovieId(@Param("id") int id);

	@Query(value = "Select * from movie_show s where s.theatre_id = :id", nativeQuery = true)
	List<Show> findShowsByTheatreId(int id);

	
}
