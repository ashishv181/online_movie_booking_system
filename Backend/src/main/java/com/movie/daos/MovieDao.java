package com.movie.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.movie.entities.Movie;

public interface MovieDao extends JpaRepository<Movie, Integer> {

}
