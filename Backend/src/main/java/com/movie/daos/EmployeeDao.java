package com.movie.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.movie.entities.Employee;

public interface EmployeeDao extends JpaRepository<Employee, Integer>{

	Employee findByemployeeEmail(String email);

}
