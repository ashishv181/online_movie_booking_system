package com.movie.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.movie.entities.Theatre;

public interface TheatreDao extends JpaRepository<Theatre, Integer> {

	@Query(value = "Select * from movie_show s where s.theatre_id = :id", nativeQuery = true)
	List<Theatre> findAllTheatresByShowId(int id);
	
}
