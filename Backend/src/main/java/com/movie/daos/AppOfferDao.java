package com.movie.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.movie.entities.AppOffer;



public interface AppOfferDao extends JpaRepository<AppOffer, Integer> {

	

}
