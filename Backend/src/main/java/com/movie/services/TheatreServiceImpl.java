package com.movie.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.movie.daos.TheatreDao;
import com.movie.entities.Theatre;

@Service
@Transactional
public class TheatreServiceImpl {
	
	@Autowired
	private TheatreDao theatreDao;


	public List<Theatre> findAllTheatresByShowId(int id) {
		// TODO Auto-generated method stub
		return null;
	}


	public List<Theatre> findAll() {
		return theatreDao.findAll();
	}


	public Theatre save(Theatre theatre) {
		return theatreDao.save(theatre);
	}


	public Theatre findById(int id) {
		Optional<Theatre> theatre = theatreDao.findById(id);
		return theatre.orElse(null);
	}


	public void delete(Theatre theatre) {
		theatreDao.delete(theatre);
		
	}
	
}
