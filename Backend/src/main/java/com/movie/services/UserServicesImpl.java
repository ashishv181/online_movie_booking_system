package com.movie.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.movie.daos.UserDao;
import com.movie.entities.User;
import com.movie.model.UserModel;

@Transactional
@Service
public class UserServicesImpl {

	@Autowired
	private UserDao userDao;
	
	public User save(User user) {
		//String encPassword = passwordEncoder.encode(user.getPassword());
		//user.setPassword(encPassword);
		return userDao.save(user);
	}
	
	public User findByUserEmail(String email) {
		return userDao.findByUserEmail(email);
	}

	public User authenticate(String userEmail, String userPassword) {
		User user = findByUserEmail(userEmail);
		if(user != null) {
			if(user.getUserPassword().equals(userPassword)) {
				return user;
			}
		}
		return null;
	}
	
	public User validateInputs(UserModel modelUser, User loggedInUser) {
		System.out.println("im out here");
		if(modelUser.getUserName() != null) {
			System.out.println("im in name");
			loggedInUser.setUserName(modelUser.getUserName());
		}
		if(modelUser.getUserEmail() != null) {
			System.out.println("im in email");
			loggedInUser.setUserEmail(modelUser.getUserEmail());
		}
		if(modelUser.getUserPhone() != null) {
			System.out.println("im in phone");
			loggedInUser.setUserPhone(modelUser.getUserPhone());
		}
		if(modelUser.getUserdob() != null) {
			System.out.println("im in dob");
			try {
				loggedInUser.setUserdob(new SimpleDateFormat("yyyy-MM-dd").parse(modelUser.getUserdob()));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(modelUser.getUserPassword() != null) {
			System.out.println("im in pass");
			loggedInUser.setUserPassword(modelUser.getUserPassword());
		}
		return loggedInUser;
	}

}
