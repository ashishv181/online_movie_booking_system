package com.movie.services;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.movie.daos.AppOfferDao;
import com.movie.daos.ManagerDao;
import com.movie.entities.AppOffer;
import com.movie.entities.Manager;

@Service
@org.springframework.transaction.annotation.Transactional
public class OfferServiceImpl implements OfferService  {
	@Autowired
	private AppOfferDao appofferDao;

	@Override
	public AppOffer save(AppOffer offer) {
		
		return appofferDao.save(offer);
	}

	@Override
	public List<AppOffer> findAll() {
		
		return appofferDao.findAll();
	}

	@Override
	public void deleteById(int id) {
		appofferDao.deleteById(id);
		
	}

	@Override
	public AppOffer getById(int id) {
		Optional<AppOffer> offer = appofferDao.findById(id);;
		return offer.orElse(null);
		
		
	}

	@Override
	public AppOffer findById(int id) {
		Optional<AppOffer> appoffer = appofferDao.findById(id);
		return appoffer.orElse(null); 
	}


	
}
