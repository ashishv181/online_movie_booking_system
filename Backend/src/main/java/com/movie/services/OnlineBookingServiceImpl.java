package com.movie.services;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.movie.daos.OnlineBookingDao;
import com.movie.entities.OnlineBooking;

@Service
@Transactional
public class OnlineBookingServiceImpl {
	
	@Autowired
	private OnlineBookingDao onlineBookingDao;

	public OnlineBooking save(OnlineBooking book) {
		return onlineBookingDao.save(book);
	}
}
