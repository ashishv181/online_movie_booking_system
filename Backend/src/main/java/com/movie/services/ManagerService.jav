package com.movie.services;

import com.movie.entities.Manager;

public interface ManagerService {

	Manager save(Manager manager);

	void delete(Manager manager);

}