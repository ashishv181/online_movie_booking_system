package com.movie.services;

import java.util.List;
import java.util.Optional;

import com.movie.entities.AppOffer;
import com.movie.entities.Manager;

public interface OfferService {

	AppOffer save(AppOffer offer);

	List<AppOffer> findAll();

	void deleteById(int id);

	AppOffer getById(int id);

	AppOffer findById(int id);

	
	

}