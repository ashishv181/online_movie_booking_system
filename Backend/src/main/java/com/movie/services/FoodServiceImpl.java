package com.movie.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.movie.daos.FoodDao;
import com.movie.entities.FoodItem;

@Service
@Transactional
public class FoodServiceImpl {
	
	@Autowired
	private FoodDao foodDao;

	public List<FoodItem> findAll() {
		return foodDao.findAll();
	}

	public FoodItem save(FoodItem item) {
		return foodDao.save(item);
	}

	public FoodItem findByName(String name) {
		return foodDao.findByFoodItemName(name);
	}

}
