package com.movie.services;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.movie.daos.AdminDao;
import com.movie.entities.Admin;
import com.movie.entities.Employee;
import com.movie.entities.User;
import com.movie.model.Credential;

@Service
@Transactional
public class AdminServiceImpl {

	@Autowired
	private AdminDao adminDao;
	
	public Admin authenticate(Credential cred) {
		Admin admin = findByAdminEmail(cred.getEmail());
		if(admin != null) {
			if(admin.getAdminPassword().equals(cred.getPassword())) {
				return admin;
			}
		}
		return null;
	}

	private Admin findByAdminEmail(String email) {
		return adminDao.findByadminEmail(email);
	}
}
