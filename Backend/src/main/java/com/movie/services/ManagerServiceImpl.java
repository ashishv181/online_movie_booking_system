package com.movie.services;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.movie.daos.ManagerDao;
import com.movie.entities.Admin;
import com.movie.entities.Manager;
import com.movie.model.Credential;

@Service
@org.springframework.transaction.annotation.Transactional
public class ManagerServiceImpl  {
	@Autowired
	private ManagerDao managerDao;

	public Manager save(Manager manager) {
		return managerDao.save(manager);
	}
	
//	public void delete(Manager manager) {
//		managerDao.delete(manager);
//	}

	public void deleteById(int id) {
		managerDao.deleteById(id);
	}

	public List<Manager> findAll() {
		return managerDao.findAll();
	}

	public Manager findById(int id) {
		Optional<Manager> manager = managerDao.findById(id);
		return manager.orElse(null);
	}
	
	public Manager authenticate(Credential cred) {
		Manager manger = findByManagerEmail(cred.getEmail());
		if(manger != null) {
			if(manger.getManagerPassword().equals(cred.getPassword())) {
				return manger;
			}
		}
		return null;
	}

	private Manager findByManagerEmail(String email) {
		return managerDao.findBymanagerEmail(email);
	}
	
}
