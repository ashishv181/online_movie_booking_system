package com.movie.services;

import java.util.List;

import com.movie.entities.Show;

public interface ShowService {

	Show save(Show Show);

	List<Show> findAll();

	Show findById(int id);

	void deleteById(int id);
	
	List<Show> findAllShowByMovieId(int id);

	List<Show> findAllShowByTheatreId(int id);
	
	

}