package com.movie.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.movie.daos.EmployeeDao;
import com.movie.entities.Employee;
import com.movie.entities.User;
import com.movie.model.Credential;

@Service
@Transactional
public class EmployeeServiceImpl {
	
	@Autowired
	private EmployeeDao employeeDao;
	
	
	public Employee authenticate(Credential cred) {
		Employee emp = findByEmployeeEmail(cred.getEmail());
		if(emp != null) {
			if(emp.getEmployeePassword().equals(cred.getPassword())) {
				return emp;
			}
		}
		return null;
	}


	private Employee findByEmployeeEmail(String email) {
		return employeeDao.findByemployeeEmail(email);
	}


	public List<Employee> findAll() {
		return employeeDao.findAll();
	}


	public Employee save(Employee emp) {
		return employeeDao.save(emp);
	}


	public Employee findById(int id) {
		Optional<Employee> emp = employeeDao.findById(id);
		return emp.orElse(null);
	}


	public void delete(Employee emp) {
		employeeDao.delete(emp);
		
	}
}	
