package com.movie.services;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.movie.daos.OfflineBookingDao;
import com.movie.entities.OfflineBooking;

@Service
@Transactional
public class OfflineServiceImpl {

	@Autowired
	private OfflineBookingDao offlineBookingDao;

	public OfflineBooking save(OfflineBooking book) {
		return offlineBookingDao.save(book);
	}
}
