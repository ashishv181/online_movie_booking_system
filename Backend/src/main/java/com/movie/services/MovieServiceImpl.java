package com.movie.services;

import java.util.List;

import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.movie.daos.MovieDao;
import com.movie.entities.Movie;

@Service
@Transactional
public class MovieServiceImpl {
	
	@Autowired
	private MovieDao movieDao;
	

	public List<Movie> findAll() {
		return movieDao.findAll();
	}


	public Movie save(Movie movie) {
		return movieDao.save(movie);
	}


	public Movie findById(int id) {
		Optional<Movie> movie = movieDao.findById(id);
		return movie.orElse(null);
	}


	public void delete(Movie movie) {
		movieDao.delete(movie);
	}

}
