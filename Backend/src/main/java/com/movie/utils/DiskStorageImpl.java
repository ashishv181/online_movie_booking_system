package com.movie.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

@Component
public class DiskStorageImpl {
//	@Value("${disk.upload.basepath}")
	private static String BASEPATH = "D:/Study/ProjectImages/food-images";

	public static String store(MultipartFile mpFile) {
		String fileName = UUID.randomUUID().toString().replaceAll("-", "");
		File filePath = new File(BASEPATH,fileName);
		
		try(FileOutputStream out = new FileOutputStream(filePath)){
			FileCopyUtils.copy(mpFile.getInputStream(), out);
			return fileName;
		}catch (IOException e) {
			e.printStackTrace();
		}
		return null;	
	}
	
	public static Resource load (String fileName) {
		File filePath = new File(BASEPATH,fileName);
		if(filePath.exists()) {
			return new FileSystemResource(filePath);
		}
		return null;
	}
	

}
